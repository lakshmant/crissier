<?php

use App\Psr4AutoloaderClass;

if(!class_exists('App\Psr4AutoloaderClass')) {

    require __DIR__ . '/src/autoloader.php';
    Psr4AutoloaderClass::getInstance()->addNamespace('Procab', __DIR__.'/src/Procab');
    Psr4AutoloaderClass::getInstance()->addNamespace('App', __DIR__.'/src/App');

}

$include_files = [
   'src/helpers.php',
    'src/setup.php',
    'src/filters.php',
    'src/Procab/Wp/Wrapper.php',
    'src/cf7-filter.php'
];

array_walk($include_files, function($file){
    if(!locate_template($file, true, true)){
        trigger_error(sprintf('Couldnot find %s', $file), E_USER_ERROR);

    }
});
unset($include_files);
/*
 * social share function
 */
function app_social_share($permalink,$title,$fav) {
    $emailContent = 'subject=' . urlencode("J'aimerais partager un lien avec vous") . '&body=' . urlencode($permalink);
    ?>
    <ul class="promo-action-list d-inline-block">
        <li class="mb-2 dropdown share-dropdown">
            <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon icon-share icon-md icon-faded mr-0"></i></a>
            <ul class="share-dropdown__menu">
                <!--app id 410871346021549-->
                <li class="dropdown-item"><button class="dropdown__link" role="button" onclick="fbShare('<?=$permalink?>')"><i class="icon icon-facebook mr-0"></i></button></li>
                <li class="dropdown-item"><a target="_blank" class="dropdown__link" href="https://twitter.com/intent/tweet?url=<?=$permalink?>&text=<?=urlencode(strip_tags($title))?>&via=centrecrissier"><i class="icon icon-twitter mr-0"></i></a></li>
                <li class="dropdown-item"><a target="_blank" class="dropdown__link" href="https://www.linkedin.com/shareArticle?url=<?=$permalink?>&title=<?=urlencode(strip_tags($title))?>"><i class="icon icon-linkedin mr-0"></i></a></li>
                <?php /*<li class="dropdown-item"><a href="mailto:?<?=$emailContent?>"><i class="icon icon-newsletter mr-0"></i></a></li>*/ ?>
                <li class="dropdown-item"><button class="dropdown__link" role="button" onclick="jQuery(this).parent().parent().parent().parent().next().find('.st_email_large').click()"><i class="icon icon-newsletter mr-0"></i></button></li>
            </ul>
        </li>
        <?php if(!empty($fav) && $fav=='yes') : ?>
            <li><a href=""><i class="icon icon-heart-outline icon-md icon-faded mr-0"></i></a></li>
        <?php endif; ?>
    </ul>
    <div style='z-index:3; position:relative; display:none;'>
        <span class="st_email_large" st_url="<?=$permalink ?>" st_title='<?=strip_tags($title)?>' displayText="Email"></span>
    </div>
    <?php /*<div class="sharethis-inline-share-buttons" ></div>*/ ?>
    <?php
}

/*
 * get the related terms post
 */

function get_related_term_post_permalink($termID) {
    $args = array(
        'post_type' => 'shop',
        'tax_query' => array(
            array(
                'taxonomy' => 'logo',
                'field' => 'term_id',
                'terms' => $termID
            ),
        ),
        'posts_per_page' => 1
    );
    $query = new WP_Query($args);
    while ($query->have_posts()): $query->the_post();
        $shopPermalink = get_permalink();
    endwhile;
    wp_reset_postdata();

    return $shopPermalink;
}

/*
 * Search in post, page,shop, promotion and services
 */

function procab_search( $query ) {
    if ( is_search() && $query->is_main_query() && $query->get( 's' ) ){
        $query->set('post_type', array('post', 'page', 'shop','promotions','services'));
    }
    return $query;
};

add_filter('pre_get_posts', 'procab_search');