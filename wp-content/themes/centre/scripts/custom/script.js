/*!
* @name: script.js
* @desc: includes common javascript snippet necessary to run over the page
*/

/*!
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == Caching Global DOM NODES
var $win = $(window);
var $page = $('html, body');
var $header = $('#masthead');
var headerHeight = $header.outerHeight();
var winTimer = null;
var winTimerDelay = 10;
var newsletterModalShowDealy = 2000;//~10000;
var docClickFlag = false;

// == Stuffs to be reused in other scripts

/**
* Check the device type
*/
window.isTouchDevice = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};

/**
* Check if device is touch(ipad or below ipad) or not
*/
var mblBreakpoint = null;
if(isTouchDevice()) {
	mblBreakpoint = 767;
}else {
	mblBreakpoint = 767 - 17;
}

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function(w, d, b, $) {
	/**
    * Don't write any code above this except comment because "use strict" directive won't have any effetc
    * Enable strict mode to prevent script to run in safe mode (modern way - ECMA2016)
    */
	"use strict";

	/**
	* Add user-agent as class
	*/
	d.documentElement.setAttribute('data-useragent', navigator.userAgent);

	/**
	* Code once DOM is ready
	*/
	$(function() {
		// Add class to body once DOM is ready
		$(b).addClass('page-loaded');

		// Add shown class to header after window load
		setTimeout(function() {
			$header.css({
				visibility: 'visible',
			});
		}, 500);
	});

	/**
	* Code once window loads
	*/
	$(w).on('load', function() {
		// == Add class to body on window load
		$(b).addClass('content-loaded');

		// == Show newsletter modal after 10 milliseconds
		setTimeout(function() {
			$('#newsletterModal').modal('show');
		}, newsletterModalShowDealy);
	});

	/**
	 * Add "last" class to the last section in the page to fix footer overlapping issue
	 */
	var mainNode = d.getElementById('main');
	var instaNode = d.getElementById('instagramBlock');
	if (mainNode) var lastChildOfmainNode = mainNode.querySelectorAll('section');//lastElementChild;
	var lastClass = 'last';

	if (!instaNode && lastChildOfmainNode) {
		for (var i = 0, j = lastChildOfmainNode.length; i < j; i++) {
			if (lastChildOfmainNode[i]) {
				lastChildOfmainNode[lastChildOfmainNode.length - 1].classList.add(lastClass);
			}
		}
	}

	/**
	* Add classes according to devices type (touch or not)
	*/

	// == Init deviceDetection() on page load
	deviceDetection();

	// == Build deviceDetection function
	function deviceDetection() {
		var deviceType = window.isTouchDevice();
		if (deviceType === true) {
			d.documentElement.classList.add('isTouchDevice');
			d.documentElement.classList.remove('notTouchDevice');
		}else {
			d.documentElement.classList.add('notTouchDevice');
			d.documentElement.classList.remove('isTouchDevice');
		}
	}

	/**
	* Fix promo content which flows out of the box on certain screen size
	*/
	var topnavbarContainer = d.querySelector('.topnavbar__c .container');
	var siteContainer = d.querySelector('.content .container');// .promo--bkg').firstChild;
	var siteContainerFirstPromoChild, promoContent;
	if (siteContainer && topnavbarContainer) {
		siteContainerFirstPromoChild = siteContainer.children[0];
		if (siteContainerFirstPromoChild) {
			promoContent = siteContainerFirstPromoChild.querySelector('.promo__c');
		}
	}
	if (promoContent) {
		// Call adjustContentWidth
		adjustContentWidth(promoContent, topnavbarContainer);
	}

	// == Build a function to adjust promo content alignment
	function adjustContentWidth(_node, _container) {
		var _container = _container.offsetWidth;
		var _nodeW = _node.offsetWidth;
		_node.classList.add('js-promo-c');
		_node.style.maxWidth = _container + 'px';
		// console.log('Window -- ' + _container + ' Node -- ' + _nodeW);
	}

	/**
	* Show hidden element on click
	*/
	var isPhoneClicked = false;
	var $jsPhoneNode = $('.js-show-phone');
	$jsPhoneNode.on('click', function(e) {
		// e.preventDefault();
		$(this)
		.closest('.card').find('.js-phone-text')
		.removeClass('ls-1')
		.text($(this).attr('data-phone-num'));

		if (isPhoneClicked) {
			$(this)
			.attr('href', 'tel:'+$(this).attr('data-phone-num')+'')
		}

		isPhoneClicked = true;
	});

	/**
	* Reset stuffs
	*/
	function resetMobileMenu() {
		// == Mobile menu reset
    	$(mobileMenuTrigger).removeClass('expanded');
    	$('.' + $(mobileMenuTrigger).attr('data-target')).removeClass('show');
	}

	function resetCollapsablePanel() {
		// == Collapsable panel reset	    	
    	$(togglePanel).removeClass('active');
		$('.' + $(togglePanel).attr('data-target')).slideUp({duration: 500, easing: 'easeInOutExpo'});
	}

	/**
	* Trigger opening hours dropdown
	*/
	var jsNode = document.querySelectorAll('.js-opening-hours');
    var targetNode = document.querySelector('.opening-hours-dropdown');
    $(jsNode).on('click', function(e) {
    	e.stopPropagation();
    	e.preventDefault();
        $(targetNode).toggleClass('show');

        // == Init resetMobileMenu()
		resetCollapsablePanel();

		// == Init resetMobileMenu()
		// resetCollapsablePanel();
    });

    /**
	* Collpase / Expand panel via javascript
	*/
	var togglePanel = document.querySelectorAll('.js-toggle-panel');
    $(togglePanel).each(function(e) {
    	$(this).on('click', function(e) {
    		$(this).toggleClass('active');
    		$('.' + $(this).attr('data-target')).slideToggle({duration: 500, easing: 'easeInOutExpo'});
    		e.preventDefault();

    		// == Set docClickFlag to true
			if ($(this).hasClass('active')) docClickFlag = true;
			if (!$(this).hasClass('active')) docClickFlag = false;

			// == Init resetMobileMenu()
			resetMobileMenu();
	    });
    });

    /**
	* Mobile menu javascript
	*/	
	var mobileMenuTrigger = document.querySelectorAll('.js-navbar-toggle');
    $(mobileMenuTrigger).on('click', function(e) {
		$(this).toggleClass('expanded');
		$('.' + $(this).attr('data-target')).toggleClass('show');
		e.preventDefault();

		// == Set docClickFlag to true
		if ($(this).hasClass('expanded')) docClickFlag = true;
		if (!$(this).hasClass('expanded')) docClickFlag = false;

		// == Init resetMobileMenu()
		resetCollapsablePanel();
    });

    /**
    * Bootstrap's dropdown simulation
    */
    var bs_dropDownToggle = document.querySelectorAll('.dropdown-toggle');
    $(bs_dropDownToggle).each(function() {
    	$(this).on('click', function() {
	    	// == Init resetMobileMenu()
			resetCollapsablePanel();

			// == Init resetMobileMenu()
			// resetCollapsablePanel();
	    });
    });

    // == Stop click event on mobile menu
    // var mblMenuNode = document.querySelector('.js-flyout-menu') || document.querySelector('.collapsable-panel-group');
    $('.js-flyout-menu, .collapsable-panel-group').on('click', function(e) { e.stopPropagation(); });

    /**
    * Handle on click event on document only when
    */
    $(d).on('click', function(e) {
    	if (!docClickFlag) return;
    	var container = $('.js-toggle-panel, .navbar__header');
    	// if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	    	// == Init resetMobileMenu();
	    	resetMobileMenu()

	    	// == Init resetMobileMenu()
			resetCollapsablePanel();

	    	// == Set docClickFlag to true
			docClickFlag = false;
	    }
    });

    /**
    * Media element js initialization
    */

    // Init minionsAudioVideo
	extendMediaElementJS($('.js-audio__node'));

	// Function to build custom audio-video
	function extendMediaElementJS($media) {
		if($media.length === 0) return;
		$media.each(function() {
			var $self = $(this);
			$self.mediaelementplayer();
		});
	}

    /**
    * LightGallery initialization
    */
	buildLightGallery($('.lightbox'));

	// Lightbox gallery function
	function buildLightGallery($video_node) {
		if($video_node.length === 0) return;
		$video_node.each(function() {
			var $self = $(this);
			$self.lightGallery({
				videoMaxWidth: 75 + '%'
			});
		});
	}

	/**
	* CMS stuffs
	*/

	// == Add helper classes to list that comes from editor
	detectListType($('ol, ul'));

	function detectListType(_list) {
	    if($(_list).length === 0) return;
	    $(_list).each(function() {
	        var $self = $(this);
	        if($self.attr('class') === undefined) {
	            if($self.is('ol')) {
	                $self.addClass('lists-editor lists-editor--ordered');
	            }else if($self.is('ul')) {
	                $self.addClass('lists-editor lists-editor--unordered');
	            }
	        }
	    });
	}

	/**
    * Bootstrap modal vertical align center
    */
	function bootstrapModalCenterY(element) {
		if($(element).length === 0) return;
		var $element = $(element);
		$element.each(function() {
			var $self = $(this);
			var $dialog = $self.find('.modal-dialog');
			var dialogH = $dialog.outerHeight();

			if(dialogH >= $(window).height()) {
				$element.removeClass('y-axis-center');
			}else {
				$element.addClass('y-axis-center');
			}
		});
	}

	// Call bootstrapModalCenterY(this) on page load
	bootstrapModalCenterY($('.modal'));

	// Call bootstrapModalCenterY(this) on modal show
	$('.modal').on('show.bs.modal', function () {
		bootstrapModalCenterY(this);
	});

    /**
    * Back to top of the page script
    */

	// == Init back to top function
	scrollPageTop($('.js-back-top-trigger'), $('.back-top-wrap'));
	
	// == Build a function to scroll at the top of the page
	function scrollPageTop($elem, $parent) {
	    if($elem.length === 0) return;
	    $win.on('scroll', function() {
	        var pageTop = $win.scrollTop();
	        var pageBottom = $(document).height() - $(this).height() - $(this).scrollTop();

	        if (pageTop <= 150 || pageBottom <= 50) {
	        	$elem.closest($parent).removeClass('animate');
	        }else {
	        	$elem.closest($parent).addClass('animate');
	        }
	    });

	    //Attach click event on current object
        $elem.on('click', function(e) {
            $page.animate({scrollTop: 0}, {duration: 700, easing: 'easeInOutExpo'});
            // Prevent the default behavior of the anchor tag
            e.preventDefault();
        });
	}

	/**
	* Placeholder script
	*/
	window.placeholderEffect = function(input)  {
	    if($(input).length === 0) return;
	    input.each( function(){
	        var meInput = $(this);
	        var placeHolder = $(meInput).attr('placeholder');
	        $(meInput).focusin(function(){
	            $(meInput).attr('placeholder','');
	        });
	        $(meInput).focusout(function(){
	            $(meInput).attr('placeholder', placeHolder);
	        });
	    });
	}

	// == Placeholder initialization
	window.placeholderEffect($('.form-control'));

	/*
	** Add heloper class on DOM element on window resize
	*/
	function activateResizeHandler() {	    
	    var resizeClass = 'resize-active',
	    flag, timer;

	    var removeClassHandler = function() {
	      flag = false;
	      d.documentElement.classList.remove(resizeClass);

	    };

	    var resizeHandler = function() {
	      if(!flag) {
	        flag = true;
	        d.documentElement.classList.add(resizeClass);
	      }
	      clearTimeout(timer);
	      timer = setTimeout(removeClassHandler, 10);
	    };
	    $(w).on('resize orientationchange', resizeHandler);
  	};

  	// == activateResizeHandler initialization
	activateResizeHandler();

	/**
	* Scroll to target on page load
	*/
	var getPageUrl = window.location.hash; // Get hash value from url
	if (getPageUrl) {
		$page.animate({scrollTop: $(getPageUrl).offset().top - headerHeight}, {duration: 700, easing: 'easeInOutExpo'});
	}


	/**
	 * @reminder :: IE doesn't support forEach constructor, so please add it on prototype of the array
	 */
	
	Array.prototype.forEach.call(document.querySelectorAll('a'), function(element, index) {
		element.onclick = function() {
			if ($('#' + this.href.split('#')[1]).length === 0) return;
			if (this.href === '#') return;
			if (this.href.indexOf('#') > -1) {
				$page.animate({scrollTop: $('#' + this.href.split('#')[1]).offset().top - headerHeight}, {duration: 700, easing: 'easeInOutExpo'});
			}
		};
	});

  	/**
  	* Recalculate calculations on window resize
  	*/
	$(w).on('resize', function() {
		if(winTimer != null) clearTimeout(winTimer);
		winTimer = setTimeout(function() {
			// Call bootstrapModalCenterY(this) on window resize
			if ($('.modal.in').length != 0) {
				bootstrapModalCenterY($('.modal.in'));
			}

			// == Re-Init deviceDetection() on page load
			deviceDetection();

			// Update header height on window resize
			headerHeight = $header.outerHeight();

			// == Readjust promo content alignment
			if (promoContent) {
				// Call adjustContentWidth
				adjustContentWidth(promoContent, topnavbarContainer);
			}
		}, winTimerDelay);
	});
})(window, document, 'body', jQuery);

/**
* Wow plugin initialization
*/
;(function() {
	wow = new WOW({
		boxClass:     'wow',      // default
		animateClass: 'animated', // default
		offset:       0,          // default
		mobile:       false,       // default
		live:         true        // default
	});
	wow.init();
})();