/*!
* @name: jquery-sliders.js
* @desc: creating slider using swiper's library
*/

/*!
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// Wrap javascript code inside a clouser to avoid possible conflicts with other codes
;(function($) {
	// == Enable strict mode using use-strict directive
	"use strict";

	// == Initialize standalone slider instance
	initHeroSlider($('.js-slider-hero'));

	// == Build standalone slider
	function initHeroSlider($node) {
		if($node.length === 0) return;
		$node.each(function() {
			var $self = $(this);

			var swiper = new Swiper($self, {
				autoplay: {
					delay: 5000,
					disableOnInteraction: false
				},			
				autoHeight: true,
				loop: true,
				speed: 700,
				navigation: {
	                nextEl: '.swiper-button-next',
	                prevEl: '.swiper-button-prev',
	            },
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					type: 'bullets',
				},
			});
		});
	}

	// == Initialize initCarousel instance
	initCarousel($('.js-carousel'));

	// == Build Carousel
	function initCarousel($node) {
		if($node.length === 0) return;
		$node.each(function() {
			var $self = $(this);

			var swiper = new Swiper($self, {
				autoplay: {
					delay: 5000,
					disableOnInteraction: false
				},
				autoHeight: true,
				loop: true,
				speed: 700,
				navigation: {
	                nextEl: '.swiper-button-next',
	                prevEl: '.swiper-button-prev',
	            },
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					type: 'bullets',
				},
				slidesPerView: 3,
                slidesPerGroup: 3,
                spaceBetween: 30,
                breakpoints: {
                    767: {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 20,
                    },
                    399: {
                        slidesPerView: 1,
                        slidesPerGroup: 1,
                        spaceBetween: 0,
                    }
                },
			});
		});
	}
})(jQuery); // Self invoking function - IIFE