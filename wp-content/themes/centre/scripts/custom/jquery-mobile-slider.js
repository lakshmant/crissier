/*
** @plugin: swiper.min.js
** @module: mobile slider with swiper.min.js
*/

// Wrap function with a javascript clousure to avoid potential code conflict with other libraries and codes
;(function($) {
    // Cache DOM nodes(elements) into jQuery objects to prevent frequent DOM traverse
    var mySwiper = undefined;
    var mblBreakpoint = window.mblBreakpoint;

    // Swiper plugin initialization :: initJsSliderSmallScreen();
    initJsSliderSmallScreen($('.js-slider-sm'));

    //Swiper plugin initialization on window resize :: initJsSliderSmallScreen();
    $win.on('resize', function(){
        initJsSliderSmallScreen($('.js-slider-sm'));
    });

    // Build slider in smaller screen
    function initJsSliderSmallScreen($node) {
        if($node.length === 0) return;
        var winWidth = $(window).width();
        if(winWidth <= mblBreakpoint && mySwiper == undefined) {            
            $node.each(function(){
                var $self = $(this);

                mySwiper = new Swiper(this, {
                    autoplay: {
                        delay: 5000,
                        disableOnInteraction: false,
                    },
                    loop: true,
                    slidesPerView: 1,
                    autoHeight: true,
                    speed: 700,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                        clickable: true
                    },
                    spaceBetween: 0
                });
            });
        } else if (winWidth > mblBreakpoint && mySwiper !== undefined) {            
            $node.each(function(){
                mySwiper = this.swiper;
                // Reset slider by destroying the current instance
                //this.swiper.wrapper.removeAttr('style');
                
                this.swiper.slides.removeClass('swiper-slide-duplicate-active swiper-slide-duplicate-next swiper-slide-duplicate-prev swiper-slide-prev swiper-slide-active swiper-slide-next').removeAttr('style');

                mySwiper.destroy();
                mySwiper = undefined;
            });
        }        
    }
})(jQuery); // Self invoking function - IIFE