<?php

/**

 * The template for displaying 404 pages (not found)

 *

 * @package WordPress

 * @subpackage Twenty_Fifteen

 * @since Twenty Fifteen 1.0

 */

?>

<!-- Site main content wrapper -->
<main id="main" class="main" role="main">
	<div class="content">
		<div class="container position-static">

			<!-- Reusable promotion block :: #type_2 -->
			<section class="promo promo--text text-center bg-primary mask-overlay hide-xs-mask is-extended-full wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
				<figure class="promo__pic mb-0 py-6 py-sm-0"><img alt="Promo Image" class="img img-full img-fluid hidden-xs-down" src="<?php echo get_template_directory_uri(); ?>/contents/banner-sm.jpg" /></figure>
				<div class="promo__c is-floated center position-static">
					<div class="h1 text-white mb-0">404 page</div>
				</div>
			</section><!-- /.Reusable promotion block ends -->

			<!-- Reusable simple text block -->
			<section class="block block--noresults gap-p-sm bg-white last is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
				<div class="text-container--sm">
					<header class="block__h">
						<h3 class="fw-m mb-2 pb-2">NAVRÉ, LA PAGE DEMANDÉE N'EXISTE PAS OU PLUS.</h3>
					</header>
					<div class="block__b">
						<p>Si vous êtes perdu, nous vous invitons à utiliser ce champ de recherche.</p>
					</div><!-- /.Block body ends -->
				<?php /*
					<div class="form search-form mt-3 mt-4">
						<form>
							<div class="form-group mb-0 position-relative">
								<input class="form-control border-fat border-primary em" type="text" placeholder="Rechercher sur le site" />
								<button type="submit" class="form__btn is-floated"><i class="icon icon-primary icon-md icon-search mr-0"></i></button>
							</div>
						</form>
					</div>
				</div>
 				*/ ?>
			</section><!-- /.Reusable simple text block ends -->

		</div><!-- /.Site container ends -->
	</div><!-- /.Site content ends -->
</main><!-- /.Site main content wrapper ends -->
