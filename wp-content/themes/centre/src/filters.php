<?php
/**
 * Created by PhpStorm.
 * User: laxman
 * Date: 7/8/2017
 * Time: 9:47 PM
 */

/**
 * add wrapper - template inheritance in wp
 */
add_filter('template_include', ['Procab\\Wp\\Wrapper', 'wrap'], 200);


function procab_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'RJ' ), max( $paged, $page ) );
    }

    return $title;
}
add_filter( 'wp_title', 'procab_wp_title', 10, 2 );

//remove stress characters, stress characters

add_filter('wp_handle_upload_prefilter', 'procab_upload_filter',1,1 );
function procab_upload_filter( $file ){
    $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/','-', $file['name']);
    return $file;

}
/*
 * Redirect to home page if author is 1
 */

add_action('template_redirect', 'procab_template_redirect');
function procab_template_redirect(){
    if (is_author()){
        wp_redirect( home_url() ); exit;
    }
}


remove_filter( 'the_content', 'wpautop' );

add_filter( 'the_content', 'wpautop' , 12);



add_filter('excerpt_more', function($more){
    return '...';
});

add_filter('excerpt_length', function($length){

    if (is_search () ) {
        return 26;
    } else {
        return 12;
    }

});



/**
 * add custom css classes to the editor
 * http://www.wpbeginner.com/wp-tutorials/how-to-add-custom-styles-to-wordpress-visual-editor/
 * //todo create standalone css group buttons instead of format
 */

//


/**
 * Generate custom search form
 *
 * @param string $form Form HTML.
 * @return string Modified form HTML.
 * */

function procab_search_form( $form ) {

    $form ='<form  method="get" action="' . home_url( '/' ) . '">
                        <div class="form-group mb-0 position-relative">
                            <input type="text" name="s" class="form-control form-control--lg border-fat border-primary" placeholder="Rechercher sur le site" value="' . get_search_query() . '" />
                            <button type="submit" class="form__btn is-floated"><i class="icon icon-primary icon-md icon-search mr-0"></i></button>
                        </div>
                    </form>';
    return $form;
}
add_filter( 'get_search_form', 'procab_search_form' );



/*
 * Modify Login
 */

function procab_login_logo() {
    echo '<style type="text/css">
        @import('.get_bloginfo( 'template_directory' ) .'/assets/fonts/admin-login-css/style.css);
        
        body{
            background: #f1f1f1;
        }
        .login h1 a { background-image:url('. get_bloginfo( 'template_directory' ) .'/assets/images/logo.png) !important;background-size: 82% !important; margin-bottom: 20px; height: 105px; width: 162px; }
        .login h1 a:hover,
        .login h1 a:focus {
            box-shadow: none;
            outline: 0;
        }
        
        #loginform {
            background: #e1568e;            
        }

        #loginform .button-primary {
            background-color: #263E9B;
            border-color: #263E9B;
            box-shadow: none;
            transform: none;
            text-shadow: none;
        }
        #loginform .button-primary:focus,
        #loginform .button-primary:active
        #loginform .button-primary:active:focus {
            box-shadow: none;
            transform: none;
        }
        .login form .input, .login input[type=text], input[type=password] {
            display: block;
            margin-top: 10px;
            padding: 10px 20px;
            background: #e1568e;
            border: 1px solid #ffffff;
            color: #ffffff;
            font-size: 16px;            
        }
        .login form .input:focus, .login input[type=text]:focus, input[type=password]:focus {
            outline: 0;
            box-shadow: none;
        }
        .login label {        
            color: #ffffff;
            font-size: 16px;
        }
        input[type=text]:focus, input[type=password]:focus {
            outline: 0;
        }
        input[type=checkbox] {
            border-color: #e1568e !important;
            box-shadow: none !important;
            outline: 0 !important;
        }
        input[type=checkbox]:checked:before {
            color: #e1568e;
        }
        input:-webkit-autofill {
            -webkit-text-fill-color: #ffffff !important;
            -webkit-box-shadow: 0 0 0 1000px #e1568e inset !important;
        }
        input::-webkit-input-placeholder { / Chrome/Opera/Safari /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input::-moz-placeholder { / Firefox 19+ /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input:-ms-input-placeholder { / IE 10+ /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input:-moz-placeholder { / Firefox 18- /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }

        .login a {
            color: #263E9B;
            transition: all linear 0.35s;
        }

        .login #nav a ,
        .login #backtoblog a {
            color: #263E9B;
            font-size: 14px;
            font-weight: 600;
        }

        .login #backtoblog a {
            color: #263E9B;
        }

        .login #nav a:hover,
        .login #nav a:focus,
        .login #backtoblog a:hover,
        .login #backtoblog a:focus {
            color: #263E9B;
            box-shadow: none;
            outline: 0;
        }

        .login form .forgetmenot label {
            font-size: 14px;
        }

    </style>';
}
add_action('login_head', 'procab_login_logo');

/*
 * Audio
 */

/*
 * Modifying the exisitng Media Player
 */

add_filter( 'wp_audio_shortcode_override', 'kd_audio_shortcode', 10, 4 );
function kd_audio_shortcode( $return = '', $attr, $content, $instances )
{
    # If $return is not an empty array, the shorcode function will stop here printing OUR HTML
    // $return = '<html code here>';
    if(!empty($attr['mp3'])) :

        $audioContents = ' <div class="audio-wrap js-audio-wrap">
                            <audio controls="controls" preload="auto" class="js-audio__node">
                                <!-- <source src="audio/minions-clip.ogg" type="audio/ogg" /> -->
                                <source src="'.$attr['mp3'].'" type="audio/mpeg" /></audio></div>';
    endif;

    return $audioContents;
};


function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
   // var_dump($buttons);
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');


/*
* Callback function to filter the MCE settings
*/

function app_mce_before_init_insert_formats( $init_array ) {

    $style_formats = [

        [
            'title' => 'Pink Box Button Class',
            'selector' => 'a',
            'classes' => 'btn btn-outline-primary ls-1 text-uppercase'
        ],
        [
            'title' => 'Black Box Button Class',
            'selector' => 'a',
            'classes' => 'btn btn-outline-dark ls-1 text-uppercase'
        ],

       /* [
            'title' => 'Black Text, Pink Box Button Class',
            'selector' => 'a',
            'classes' => 'btn btn-outline-primary'
        ],*/
        [
            'title' => 'Black Background PDF',
            'selector' => 'ul',
            'classes' => 'button-utilities__list button-utilities__list--pdfblack'
        ],

        [
            'title' => 'White Background PDF',
            'selector' => 'ul', // Element to add class to
            'classes' => 'button-utilities__list button-utilities__list--pdfwhite'
        ]

    ];
    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'app_mce_before_init_insert_formats' );


/*
 * Exclude pages from WordPress Search
 */

if (!is_admin()) {
    function app_search_filter($query) {
        if ($query->is_search) {
            $query->set('post_type', 'post');
        }
        return $query;
    }
    add_filter('pre_get_posts','app_search_filter');
}