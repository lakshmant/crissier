<?php
class Procab_FileCacheContainer{
    /**
     * 
     * @var integer time to live
     */
    protected $ttl;
    protected $group;
    public function __construct($ttl, $group = ''){
        $this->ttl = $ttl;
        $this->group = $group;
    }
    
    public function captureStart(){
        ob_start();
    }
    
    public function getGroup(){
        return $this->group;
    }
    
    public function getTTL(){
        return $this->ttl;
    }
    
    
    public function captureEnd(){
        $data = ob_get_clean();
        return $data;
    }
    
}