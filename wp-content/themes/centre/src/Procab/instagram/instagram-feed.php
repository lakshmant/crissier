<?php
require __DIR__.'/../lib/Procab/FileCache.php';
require __DIR__.'/../lib/Procab/FileCacheContainer.php';

$fileCache = new Procab_FileCache(__DIR__.'/../../cache/',true);
$fileCacheKey = 'instagram';
$cacheContent = $fileCache->restore($fileCacheKey);
if(!$cacheContent){
    //echo 'not found in cache!';

    $instaResult= file_get_contents('https://www.instagram.com/procabstudio/?__a=1');
    try{
        $fileCache->captureStart($fileCacheKey);

        $insta = json_decode($instaResult);
		
		//print_r( $insta);
        //if(count($insta->user->media->nodes)){
            //$insta->items = array_slice($insta->items, 0, 3);
			$nodes = array_slice($insta->user->media->nodes, 0, 3);
            foreach($nodes as $node):
                $imageUrl = $node->thumbnail_src;
				//$link = $item->link;
                ?>
                <div class="img-insta">
					<a href="https://www.instagram.com/procabstudio/" target="_blank"><img src="<?=$imageUrl?>" /></a>
				</div>
            <?php
            endforeach;
        //}

        echo $fileCache->captureEnd($fileCacheKey);

    }catch(Exception $e){
        //error
        //incase the file_get_content failed!
    }
    //

}else {
    //echo 'from cache';
    echo $cacheContent;
}