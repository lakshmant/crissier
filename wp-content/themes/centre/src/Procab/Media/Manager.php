<?php
namespace Procab\Media;

use Intervention\Image\ImageManager;

class Manager{

    /**
     * @var ImageManager
     */
    protected $resizer;
    protected $saveFolder;
    protected $saveUrl;
    protected $quality = 60;

    /**
     * Manager constructor.
     * @param ImageManager $resizer
     * @param array $config
     */
    public function __construct($resizer, array $config)
    {
        $this->resizer = $resizer;
        $this->setConfig($config);
        return $this;
    }

    protected function setConfig(array $config)
    {
        if(isset($config['sizes']) && is_array($config['sizes'])){
            $this->setSizes($config['sizes']);
        }else{
            throw new \Exception('Sizes not set!');
        }

        if(isset($config['save_folder'])){
            $this->saveFolder = $config['save_folder'];
        }else{
            throw new \Exception('Save folder not set!');
        }

        if(isset($config['save_url'])){
            $this->saveUrl = $config['save_url'];
        }else{
            throw new \Exception('Save url not set!');
        }
    }

    public function setSizes(array $sizes)
    {
        $this->sizes = $sizes;
    }

    protected function sanitizeFileName($fileName)
    {
        return preg_replace('/[^a-zA-Z0-9-_\.]/','-', $fileName);
    }

    protected function getResizer()
    {
        return $this->resizer;
    }

    public function resize($src, $size)
    {
        $dimension = [];
        if(!file_exists($src) || !is_file($src)){
            return '';
        }
        //todo check if file is image

        if(array_key_exists($size, $this->sizes)){
            //var_dump('size not found!');
            $dimension = $this->sizes[$size];
            //return $src;
        }

        //$newFileName = $size.'-'.$this->sanitizeFileName(basename($src));
        $newFileName = $this->sanitizeFileName(basename($src));

        if($dimension){
            //$newFileName = $size.'-'.$newFileName;
            $newFileName = 'op_'.join('_', array_filter($dimension)).'-'.$newFileName;
            //$newFileName = 'op_'.join('_', $dimension).'-'.$newFileName;
        }
        $savePath = $this->saveFolder.'/'.$newFileName;

        if(file_exists($savePath)){
            //skip
        }else{
            try{
                if($dimension){
                    if($dimension[2] == 'resize'){
                        $this->getResizer()->make($src)->resize($dimension[0], null, function ($constraint) {
                            $constraint->aspectRatio();
                            //$constraint->upsize();
                        })->save($savePath, $this->quality);
                    }else{
                        $this->getResizer()->make($src)->fit($dimension[0], $dimension[1])->save($savePath, $this->quality);
                    }
                }else{
                    $this->getResizer()->make($src)->save($savePath, $this->quality);
                }

            }catch (\Exception $e) {
                return '';
            }
        }
        return $this->saveUrl.'/'.$newFileName;
    }
}