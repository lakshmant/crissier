<?php

add_action("wpcf7_before_send_mail", "promotion_send_email", 10, 1);
function promotion_send_email($cf7) {
    // get the contact form object
    if($cf7->id() == 421) {
        $wpcf = WPCF7_ContactForm::get_current();
        // get current SUBMISSION instance
        $submission = WPCF7_Submission::get_instance();
        if($submission) {
            // get submission data
            $data = $submission->get_posted_data();
            // nothing's here... do nothing...
            if (empty($data))
                return;
            $reply_email = $data['email'];

            $mailProp['mail']['recipient'] = 'himalisov@gmail.com,'.$reply_email;
            $wpcf->set_properties(array('mail' => $mailProp['mail']));
        }
    }
    return $wpcf;
}

add_filter('wpcf7_mail_components','procab_custom_mail', 10,2);
function procab_custom_mail($mail_component, $contact_form){
    $myID = 421;
    $id = $contact_form->id;
    if($id==$myID) :
        $mail_component['subject'] ="Promotion Detais"; //email subject
        $mail_component['sender']; //sender field (from)
        $mail_component['body']; //email body
        $mail_component['recipient']; //email recipient (to)
        $mail_component['additional_headers']; //email headers, cc:, bcc:, reply-to:
        $mail_component['attachments']; //file attachments if any

        $mail_component['additional_headers'] = "MIME-Version: 1.0" . "\r\n";
        $mail_component['additional_headers'] .= "Content-type:text/html;charset=UTF-8" . "\r\n";



        $key_values = array();
        $tags = $contact_form->scan_form_tags(); //get your form tags
        foreach($tags as $tag){
            $field_name  = $tag['name'];
            if(isset($_POST[$field_name]) && !empty($_POST[$field_name])){
                //get all the submitted fields form your form
                $key_values[$field_name] = $_POST[$field_name];
            }
        }

        global $post;
        if(!empty($key_values["promotion-id"])) :
             $promotionID = $key_values["promotion-id"];
            $logoTermID = get_the_terms($promotionID,'logo');
            if(!empty($logoTermID[0]->term_id)) :
                $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
            endif;
            $title1 = strip_tags(get_the_title($promotionID));
            if(!empty($title1)) :
                $titl1 = 'Title 1: '.$title1.'<br/>';
            endif;
            $title2 = 'Title 2 : '.strip_tags(get_field('promotion_title_2',$promotionID)).'<br/>';

            if(!empty($title2)) :
                $titl2 = 'Title 1: '.$title1.'<br/>';
            endif;

            $price = get_field('price',$promotionID);
            if(!empty($price)) :
                $price = 'Price : '.$price.'<br/>';
            endif;
        endif;

        $filename = get_attached_file($promoLogoImage['id']) ;
        $mail_component['attachments'] = array($filename);
        $body = $titl1.$title2.$price;
        $mail_component['body'] = $body;
    endif;
    return $mail_component;
}