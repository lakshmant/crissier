<main id="main" class="main" role="main">
    <div class="content">
        <div class="container position-static">
			<section class="block block-static-text bg-white gap-p-eq is-extended wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
				<header class="block__h text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
					<h2 class="block__t mb-4 mb-sm-5"><?php the_title(); ?></h2>
				</header>
                <?php
				if (have_posts()) :
					while(have_posts()) : the_post(); ?>

						<style>
							.block-static-text h3 {
								margin: 35px 0 30px;
							}

							.block-static-text h3:first-child {
								margin-top: 0;
							}

							.block-static-text h3:last-child {
								margin-bottom: 0;
							}

							a[href^="tel"],
							a[href^="mailto"] {
								text-decoration: underline;
							}

							hr {
								margin-top: 50px;
								margin-bottom: 50px;
							}
						</style>

						<?php if (!empty($post->post_content)) : ?>
							<div class="block__b">
								<div class="text-card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20"">
									<?php the_content(); ?>
								</div>
							</div>
						<?php endif; ?>
					<?php endwhile;
				endif; ?>
			</section>
			<?php
			if (have_rows('flexible_contents')):
				while (have_rows('flexible_contents')) : the_row();
					$layout = get_row_layout();
					$tpl = get_template_directory() . '/partials/flexible-contents/' . $layout . '.php';
					if (file_exists($tpl)) {
						include $tpl;
					}
				endwhile;
			endif;
			?>
    	</div><!-- /.Site container ends -->
    </div>
</main>