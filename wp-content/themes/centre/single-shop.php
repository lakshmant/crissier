<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 1/25/2018
 * Time: 2:42 PM
 */
?>

<!-- Site main content wrapper -->
<main id="main" class="main" role="main">
    <div class="content">
        <?php
        $bannerBackground = get_field('banner_image');
        $phoneNumber = get_field('phone_number');
        $openingHours = get_field('opening_hours');
        $websiteLink = get_field('website_link');
        //   if($bannerBackground) $bannerBackground = \App\getImageManager()->resize( \App\getImageDirectoryPath($bannerBackground), \App\IMAGE_SIZE_SHOP_BANNER);
        $bannerBackground = !empty($bannerBackground)? $bannerBackground : "";
        $logoTermID = get_the_terms($post->ID,'logo');

        if(!empty($logoTermID[0]->term_id)) :
            $logo = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
        endif;

        /* for displaying promotion related to promotion */
        $logoTermArray =[];
        if(!empty($logoTermID)) {
            foreach ($logoTermID as $logoID) {
                array_push($logoTermArray, $logoID->term_id);
            }
        }


        ?>
        <section class="promo text-center gap-p-sm has-cover is-parallaxed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image:url(<?=$bannerBackground?>);">
            <div class="promo__c promo__c--lg mx-auto">
                <div class="row d-flex justify-content-end">
                    <div class="col-sm-6 col-md-5">
                        <div class="no-gutters d-flex flex-wrap">
                            <?php if(!empty($logo)): ?>
                                <div class="col-6">
                                    <div class="card card--sq overflow-visible has-hover bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="-20">
                                        <div class="card__c center">
                                            <figure class="card__pic mb-0"><img alt="Brand Logo" class="img img-fluid" src="<?=!empty($logo['url'])?$logo['url']:''?>"></figure>
                                        </div>
                                        <a href="javascript:void(0);" class="link-stacked"></a>
                                        <div class="is-floated bottom">
                                            <?php app_social_share(get_permalink(),get_the_title(),''); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if(!empty($phoneNumber)):?>
                                <div class="col-6">
                                    <div class="card card--sq has-hover bg-info text-muted wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="-20">
                                        <div class="card__c center">
                                            <i class="icon icon-phone-circle icon-xl mr-0 mb-2 mb-sm-3"></i>
                                            <div class="h5 ff-pp mb-0 ls-1 js-phone-text"><?php _e("Contacter la boutique","app"); ?></div>
                                        </div>
                                        <a href="javascript:void(0);" class="link-stacked js-show-phone" data-phone-num="<?php echo $phoneNumber; ?>"></a>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-6">
                                <div class="card card--sq has-hover text-muted bg-info wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="-20">
                                    <div class="card__c center">
                                        <i class="icon icon-clock icon-xl mr-0 mb-2 mb-sm-3"></i>
                                        <div class="h5 ff-pp mb-0 ls-1"><?php _e("Consulter les horaires","app"); ?></div>
                                    </div>
                                    <a href="javascript:void(0);" class="link-stacked js-opening-hours"></a>
                                </div>
                            </div>
                            <?php if(!empty($websiteLink)): ?>
                                <div class="col-6">
                                    <div class="card card--sq has-hover bg-white text-muted wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="-20">
                                        <div class="card__c center">
                                            <i class="icon icon-desktop icon-xl mr-0 mb-2 mb-sm-3"></i>
                                            <div class="h5 ff-pp mb-0 ls-1"><?php _e("voir notre site internet","app");?></div>
                                        </div>
                                        <a href="<?=$websiteLink?>" class="link-stacked"></a>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if(empty($logo) || empty($phoneNumber) || empty($websiteLink)) : ?>
                                <div class="col-6 is-opaque hidden-xs-down">
                                    <div class="card card--sq"></div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.Prom content ends -->
        </section><!-- /.Reusable promotion block ends -->

        <div class="container position-static">
            <?php
            if (have_rows('shop_flexible_contents')):
                while (have_rows('shop_flexible_contents')) : the_row();
                    $layout = get_row_layout();
                    if($layout=='one_third_column') :
                        $tpl = get_template_directory() . '/partials/flexible-contents/' . $layout . '.php';
                    else :
                        $tpl = get_template_directory() . '/partials/flexible-contents/shop/' . $layout . '.php';
                    endif;

                    if (file_exists($tpl)) {
                        include $tpl;
                    }
                endwhile;
            else : // This is just to fix the overlapping issue caused by footer
                $output = '<section class="block"></section>';
                echo $output;
            endif;
            ?>
            <section class="block block--grid last gap-p-md bg-faded  is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeInDown;">
                <div class="block__b">
                    <?php $sectionTitle = get_field('section_title'); ?>
                    <?php if(!empty($sectionTitle)): ?> <h2 class="mb-7" style="text-align: center;"><?=$sectionTitle?></h2><?php endif; ?>
                    <div class="row align-items-start">
                        <!------FROM Promotion block--------->
                        <?php
                        $args = [ 'post_type' => 'promotions',
                            'tax_query' => [
                                [
                                    'taxonomy' => 'logo',
                                    'field'    => 'term_id',
                                    'terms'    => $logoTermArray,
                                ],
                            ],
                            'posts_per_page' => 1,
                            'orderby'=>'rand',
                            'post_status'=> 'publish'];


                        $query = new WP_Query( $args );
                        if ($query->have_posts() ) :
                            while($query->have_posts()): $query->the_post();

                                $title = strip_tags(get_the_title());
                                $promoText = get_field('text');
                                $title2 = strip_tags(get_field('promotion_title_2'));
                                $permalink = get_permalink();
                                $logoTermID = get_the_terms($post->ID,'logo');
                                if(!empty($logoTermID[0]->term_id)) :
                                    $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
                                    $promoLogoImage = $promoLogoImage['url'];
                                endif;
                                $popUpContentPromotion = get_field('popup_contents');
                                $shopPermalink =   get_related_term_post_permalink($logoTermID[0]->term_id);

                                ?>
                                <div class="col-sm-4 d-flex mb-3">
                                    <div class="card card--brand has-floating-footer bg-white shadowed wow fadeInDown text-center" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeInDown;">
                                        <div class="card-inner">
                                            <div class="meta-holder text-right">
                                                <?php app_social_share($permalink,$title,''); ?>
                                            </div>
                                            <div class="card__c mb-3">
                                                <?php if(!empty($promoLogoImage)): ?>
                                                <figure class="brand-pic mt-3 mb-3"><a href="javascript:void(0);">
                                                        <img alt="Instagram Image" class="img img-fluid" src="<?=$promoLogoImage?>"></a>
                                                </figure>
                                                <?php endif; ?>

                                                <?php if(!empty($title)) : ?><h2 class="h5 card__t ls-1 text-muted"><?php echo $title;?></h2> <?php endif; ?>
                                                <?php if(!empty($title2)): ?><p><?php echo $title2; ?></p><?php endif; ?>
                                            </div>

                                            <a href="" class="link-stacked" data-toggle="modal" data-target="#single-shop-promo-model"></a>
                                            <div class="card__f is-floated center-bottom">
                                                <button role="button" class="btn btn-mw btn-outline-primary border-fat d-block m-auto promo-btn js-promo-frm-btn" data-toggle="modal" data-target="#single-shop-promo-model">J’en profite</button>
                                                <?php if(!empty($shopPermalink)): ?>
                                                    <div class="mt-4">
                                                        <a href="<?=$shopPermalink?>" class="text-muted ff-pp js-promo-frm-link"><i class="icon icon-plus"></i><u>Voir la boutique</u></a>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                $popUpContentPromotion = get_field('popup_contents',$query->post->ID);
                                $modelID = 'single-shop-promo-model';
                                $ctaText = get_field('cta_text',$query->post->ID);
                                $dateOfferValidation = get_field('date_offer_condition',$query->post->ID);

                                //$modelID = 'promo-banner-modal-'.$promoCount;
                                $bannerImage = get_field('image',$query->post->ID);
                                $pdfGeneratorLink = home_url().'/services/promotion.php?id='.$query->post->ID;
                                $title =$popUpContentPromotion['title'];
                                $text = $popUpContentPromotion['text'];
                                $popupLogo = $promoLogoImage;
                                $popupDescription = $popUpContentPromotion['description'];
                                $link = $popUpContentPromotion['link'];
                                $permalink = get_permalink();
                                \App\app_promotion_layer_content($modelID ,$bannerImage ,$pdfGeneratorLink ,$title ,$text ,$popupLogo ,$popupDescription ,$permalink,$link );
                            endwhile;
                            wp_reset_postdata();

                        endif;
                        ?>

                        <?php
                        $catalog= get_field('catalog');
                        $catalogImage = $catalog['catalog_image'];
                        $subtitle = $catalog['catalog_subtitle'];
                        $date = $catalog['catalog_date'];
                        $catalogPDF = $catalog['catalog_pdf'];
                        $catalogLink = $catalog['catalog_link'];
                        if(!empty($catalogImage) || !empty($subtitle) || !empty($date) || !empty($catalogPDF)) :
                            ?>
                            <div class="col-sm-4 d-flex mb-3">
                                <div class="card card--brand card--brand-catalogue   bg-white shadowed wow fadeInDown text-center" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeInDown;">
                                    <div class="card-inner position-relative">
                                        <div class="meta-holder catalogue is-floated right">
                                            <?php app_social_share($catalogLink['url'],get_the_title(),''); ?>
                                        </div>
                                        <h3 class="promo-title pr-0 pr-sm-3 pr-md-4 pr-lg-3 mb-5"><a href="#" class="text-dark"><?php the_title(); ?></a></h3>
                                        <div class="card__c mb-3 pr-0 pr-sm-3 pr-md-4 pr-lg-3 pl-0 pl-sm-3 pl-md-4 pl-lg-3">
                                            <?php if(!empty($catalogImage)): ?>
                                                <figure class="brand-pic mt-3 mb-3">
                                                    <img alt="Post Thumbnail" class="img img-xs-full img-fluid" src="<?=$catalogImage?>">
                                                </figure>
                                            <?php endif; ?>
                                            <?php if(!empty($subtitle)): ?> <p class="card__t mb-3"><?=$subtitle?></p> <?php endif; ?>
                                            <?php if(!empty($date)): ?> <small><?=$date?></small><?php endif; ?>
                                        </div>
                                        <div class="pr-0 pr-sm-2 pr-md-3 pr-lg-2 pl-0 pl-sm-2 pl-md-3 pl-lg-2 ">
                                            <a href="<?=$catalogPDF?>" class="btn btn-mw btn-outline-primary border-fat d-block m-auto promo-btn js-promo-frm-btn" target="_blank">Voir le catalogue</a>
                                            <?php if(!empty($catalogLink)):
                                                $target = !empty($catalogLink['target']) ? 'target="_blank"':'';
                                                ?>

                                                <div class="mt-3">
                                                    <a href="<?=$catalogLink['url']?>" class="text-muted ff-pp js-promo-frm-link" <?=$target?>><i class="icon icon-plus"></i><u><?=$catalogLink['title']?></u></a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <!--FROM Mag block -->
                        <?php
                        $magID = get_field('select_mag');
                        $magID = $magID[0];
                        $categories = get_the_terms($magID,'category');
                        $catName = $categories[0]->name;
                        if(!empty($magID)):

                            ?>
                            <div class="col-sm-4 d-sm-flex mb-3">
                                <article id="article-2869" class="article-card article-card--sm shadowed bg-white wow fadeInDown " data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeInDown;">
                                    <div class="article-card-inner">
                                        <div class="card__c mb-3">
                                            <?php
                                            $imgSrc = wp_get_attachment_image_src( get_post_thumbnail_id( $magID ), 'full', false );
                                            $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($imgSrc[0]), \App\IMAGE_SIZE_NEWS_LARGE);
                                            if($img) : ?>
                                                <a href="<?php echo get_permalink($magID); ?>"><figure class="brand-pic mb-3"><a href="#;">
                                                            <img alt="Post Thumbnail" class="img img-xs-full img-fluid" src="<?=$img?>">
                                                    </figure></a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="article-card__c">
                                            <div class="article-card__c-inner position-relative">
                                                <div class="article-sharer-holder is-floated right">
                                                    <?php app_social_share(get_permalink($magID),get_the_title($magID),''); ?>
                                                </div>
                                                <?php if(!empty($catName)) ?><div class="h5 promo-cat"><a href="<?php get_permalink($magID); ?>" class="text-muted"><?=$catName?></a></div>

                                                <h2 class="promo-title pr-0 pr-sm-3 pr-md-4 pr-lg-3"><a href="<?php get_permalink($magID); ?>" class="text-dark"><?php echo get_the_title($magID); ?></a></h2>

                                                <div class="promo-tags ff-os">
                                                    <?php
                                                    $terms = get_the_terms($magID,'post_tag');
                                                    $termNumber = sizeof ($terms);
                                                    $termCount=1;
                                                    if(!empty($terms)) :
                                                        foreach ($terms as $term) {
                                                            echo '<span><a href="'.site_url().'/le-mag/?terms='.$term->term_id .'" class="promo-tags__link">'.$term->name.'</a></span>';
                                                            if($termCount<$termNumber){
                                                                echo ', ';
                                                            }
                                                            $termCount++;
                                                        }
                                                    endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="<?php echo get_permalink($magID); ?>" class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                            <i class="icon icon-light icon-plus card-icon mr-0"></i>
                                        </a>
                                    </div>
                                </article>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
        </div><!-- /.Site container ends -->
    </div><!-- /.Site content ends -->
</main><!-- /.Site main content wrapper ends -->
