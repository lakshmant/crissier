<?php
session_start(); 
if ( !isset( $_SESSION['count'] ) )
    $_SESSION['count'] = 1; else $_SESSION['count']++;
 if($_SESSION['count']>=10) {
     $_SESSION['count'] = 1;
 }
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<?php get_template_part('partials/head'); ?>
<body class="page">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2WX3HJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!--[if ltE IE 9]>
<div class="alert alert-danger ie-older-browser-message">
    <div class="ie-message-text">
        <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
            browser</a> to improve your experience.</p>
    </div>
</div>
<![endif]-->
<?php
do_action('get_header');
?>
<div class="site-outer">
    <?php get_template_part('partials/header');
             include \App\get_main_template();
    do_action('get_footer');
    get_template_part('partials/footer');
    ?>
</div><!-- /.Site outer ends -->

<?php get_template_part('partials/foot'); ?>
<?php wp_footer(); ?>

</body>
</html>