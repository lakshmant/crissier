<?php
/* Template Name: Shop Lisitng Page
 *
*/
$actionUrl = get_permalink();
//build query
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = [ 'post_type' => 'shop', 'paged' => $paged, 'paged' => $paged,'posts_per_page' => -1, 'orderby'=>'rand','post_status'=> 'publish'];
$taxQuery = [];
$searchQuery = '';
$inputCategories = [];
if(isset($_GET['cats'])) $inputCategories = array_filter($_GET['cats'], 'is_numeric');
if($inputCategories){
    $taxQuery = [
        'taxonomy' => 'category',
        'field'    => 'term_id',
        'terms'    => $inputCategories,
    ];

    $args['tax_query'] = [$taxQuery];
}
if(isset($_GET['search']) && $_GET['search'] != '') $searchInput = $_GET['search'];

if(!empty($searchInput)){
    //$args['s'] = $searchInput;

    global $wpdb;
    $searchInput = $wpdb->esc_like( $searchInput );
// Add wildcards, since we are searching within post title.
    $searchInput = '%' . $searchInput . '%';
// $db_item =  $wpdb->get_results($wpdb->prepare("SELECT ID, post_title  FROM {$wpdb->posts}  WHERE post_title LIKE %s and post_type = 'shop'",  $searchInput ));
    $searchItems =  $wpdb->get_results($wpdb->prepare("SELECT ID FROM {$wpdb->posts}  WHERE post_title LIKE %s AND post_type = 'shop' AND post_status = 'publish'",  $searchInput ),ARRAY_A);

$searchIn = [];
    foreach ($searchItems as $key => $value) {
             foreach ($value as $id) {
                 $id = intval($id);
                 array_push($searchIn,$id);
             }
    }

    if(!empty($searchIn)) :
        $args['post__in'] = $searchIn;
        else :
            $args['post__in']  = [0];
        endif;
}
$query = new WP_Query( $args );

?>
<main id="main" class="main" role="main">
    <div class="content">
        <div class="container position-static">
            <?php if(!\App\isRequestAjax()):?>
            <!-- Shop Listing page filter section -->
            <section class="block bg-primary text-center gap-p-md is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <div class="block__b">
                    <!-- Filter block -->
                    <div class="filter-holder ff-pp">
                        <div class="form">
                                <form method="get" class="filter-form" id="shop-lisitng-frm" action="<?= $actionUrl; ?>">

                                <div class="row justify-content-center">
                                    <div class="col-sm-8 col-md-6 px-md-4">
                                        <div class="form-group form-group--lg position-relative">
                                            <input type="text" name="search" class="form-control form-control--lg border-fat border-white" placeholder="Recherche par nom" />
                                            <button type="submit" class="form__btn is-floated"><i class="icon icon-light icon-md icon-search mr-0"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex flex-wrap justify-content-center filter-row">
                                    <?php
                                    $c=1;
                                    $terms = get_terms( 'category', array(
                                        'hide_empty' => false,
                                    ) );
                                    $c=1;
                                    if(!empty($terms)):
                                    foreach($terms as $category):
                                        if(!empty($inputCategories) & in_array($category->term_id, $inputCategories)) :
                                            $checkedCat = 'checked="checked"';
                                        else:
                                            $checkedCat ='';
                                            endif;


                                    ?>
                                        <div class="col-6 col-sm-3 filter-col">
                                            <div class="form-group form-group--lg position-relative">
                                                <input class="d-none filter-field js-shop-frm-input" id="filter-item0<?=$c?>" type="checkbox" name="cats[]"  value="<?=$category->term_id?>" <?=$checkedCat?> />
                                                <label for="filter-item0<?=$c?>" class="border-fat input-label-btn"><span class="input-label-text"><?=$category->name?></span></label>
                                            </div>
                                        </div>
                                    <?php $c++; endforeach; endif;?>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <script>
                    (function() {
                        window.onload = function() {
                        var $shopListing = $('#shop-lisitng-frm ');
                        $('#shop-lisitng-frm .js-shop-frm-input').change(function(){
                            $shopListing.submit();
                        });
                    };})();
            </script>
            <?php endif; ?>
            <section class="block block--grid last gap-p-md pt-0 bg-faded text-center is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <header class="block__h gap-p-xs">
                    <div class="row align-items-center">
                        <div class="col-sm-6 mb-2 mb-sm-0 text-sm-left">
                            <div class="text-muted text-uppercase ff-pp fw-m ls-1"><span class="result-text"> <?=$query->found_posts?> résultats</span></div>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0 text-sm-right">
                            <div class="text-has-icon position-relative text-primary text-uppercase ff-pp fw-m ls-1"><i class="icon icon-primary icon-price-tag"></i>*promotion en cours</div>
                        </div>
                    </div>
                </header>
                <div class="block__b">
                    <?php
                    $postCount = $query->post_count;
                    $c=1;
                    $actionUrl = get_permalink();
                    if ($query->have_posts()):
                        while($query->have_posts()):$query->the_post();
                            if($c==1) echo '<div class="row">';
                            include __DIR__.'/../partials/shop-listing-items.php';
                            $c++; if($c>$postCount) echo '</div>';
                        endwhile;
                      /*  echo "<nav class='pagination-nav mt-2 mt-sm-5'><ul class='pagination justify-content-center'>";
                        \App\custom_pagination($query->max_num_pages,"",$paged, $actionUrl);
                        echo "</ul></nav>";*/
                    else :
                        ?>
                        <p style="color: #e1568e  !important;"><?php _e('Il n\'y a pas de nouvelles basées sur vos critères de sélection. Veuillez sélectionner d\'autres critères disponibles'); ?></p>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </div>
</main>