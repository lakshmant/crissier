<?php
/* Template Name: Promotion Page
 *
*/
$actionUrl = get_permalink();
//build query
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = [ 'post_type' => 'promotions', 'paged' => $paged, 'paged' => $paged,'posts_per_page' => -1, 'orderby'=>'rand','post_status'=> 'publish'];

$taxQuery = [];
$searchQuery = '';
$inputCategories = [];

$inputShopID = $_GET['shopID'];
if(!empty($inputShopID)) {
    $logoTerms = get_the_terms($inputShopID, 'logo');
    $logoTermID =[];

    if(!empty($logoTerms)) {
        foreach ($logoTerms as $logoID) {
            array_push($logoTermID, $logoID->term_id);
        }
        if (!empty($logoTermID)) {
            $taxLogoQuery = [
                'taxonomy' => 'logo',
                'field' => 'term_id',
                'terms' => $logoTermID,
            ];

            $args['tax_query'] = [$taxLogoQuery];
        }

    }

}


if(isset($_GET['cats'])) $inputCategories = array_filter($_GET['cats'], 'is_numeric');
if($inputCategories){
    $taxQuery = [
        'taxonomy' => 'promo_cat',
        'field'    => 'term_id',
        'terms'    => $inputCategories,
    ];

    $args['tax_query'] = [$taxQuery];
}


if(isset($_GET['search']) && $_GET['search'] != '') $searchInput = $_GET['search'];
$query = apply_filters( 'get_search_query', $_GET['search'] );
$searchInput = esc_attr( $query );
$searchInput = stripslashes($query);

if(!empty($searchInput)){
    global $wpdb;
    $searchInput = $wpdb->esc_like( $searchInput );
    // Add wildcards, since we are searching within post title.
    $searchInput = '%' .$wpdb-> _real_escape($searchInput). '%';
    $searchItems =  $wpdb->get_results($wpdb->prepare("SELECT ID FROM {$wpdb->posts}  WHERE post_title LIKE %s AND post_type = 'promotions' AND post_status = 'publish'",  $searchInput ),ARRAY_A);

    $searchIn = [];
    foreach ($searchItems as $key => $value) {
        foreach ($value as $id) {
            $id = intval($id);
            array_push($searchIn,$id);
        }
    }

    if(!empty($searchIn)) :
        $args['post__in'] = $searchIn;
    else :
        $args['post__in']  = [0];
    endif;
}

$query = new WP_Query( $args );
$fullFeaturedImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID), 'full' );
?>

<main id="main" class="main" role="main">
    <div class="content">
        <div class="container position-static">
            <section class="block block-static-text bg-white gap-p-eq is-extended wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style='background-image: url("<?=$fullFeaturedImage[0]?>");'>
                <header class="block__h text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <h2 class="text-white block__t mb-4 mb-sm-5"><?php the_title(); ?></h2>
                </header>
                <?php
                if (have_posts()) :
                while(have_posts()) : the_post(); ?>

                <style>
                    .block-static-text h3 {
                        margin: 35px 0 30px;
                    }

                    .block-static-text h3:first-child {
                        margin-top: 0;
                    }

                    .block-static-text h3:last-child {
                        margin-bottom: 0;
                    }

                    a[href^="tel"],
                    a[href^="mailto"] {
                        text-decoration: underline;
                    }

                    hr {
                        margin-top: 50px;
                        margin-bottom: 50px;
                    }
                </style>

                <?php if (!empty($post->post_content)) : ?>
                <div class="block__b">
                    <div class="text-card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20"">
                    <?php the_content(); ?>
                </div>
        </div>
        <?php endif; ?>
        <?php endwhile;
        endif; ?>
        </section>
            <section class="block text-center is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <div class="block__b">
                    <div class="promo-holder">
                        <?php
                        $promotionBanner = get_field('promotion_banners');
                        $promoCount = 1;
                        if(!empty($promotionBanner)):
                            foreach ($promotionBanner as $banner) {
                                $popUpContents = $banner['popup_contents'];
                                $link = $popUpContents['link'];
                                $target =!empty($link['target'])?'target="_blank"':'';
                                ?>

                                <div class="card__c mb-3 hover">
                                    <figure class="brand-pic mt-3 mb-3" style="background-image:url('<?=$banner['banner_image']?>');">
                                        <button data-toggle="modal" data-target="#promo-banner-modal-<?=$promoCount?>" class="modal__redirect"><span class="btn btn-mw btn-outline-primary border-fat d-block m-auto promo-btn js-promo-frm-btn">J’en profite</span></button>
                                    </figure>
                                </div>

                                <?php
                                $modelID = 'promo-banner-modal-'.$promoCount;
                                $bannerImage = $banner['banner_image'];
                                $pdfGeneratorLink = home_url().'/services/promotion.php?promoCount='.$promoCount.'&id='.$post->ID;
                                $title =$popUpContents['title'];
                                $text = $popUpContents['text'];
                                $popupLogo =$popUpContents['logo'];
                                $popupDescription = $popUpContents['description'];
                                $link = $popUpContents['link'];
                                $permalink = $pdfGeneratorLink;
                                \App\app_promotion_layer_content($modelID ,$bannerImage ,$pdfGeneratorLink ,$title ,$text ,$popupLogo ,$popupDescription ,$permalink,$link );
                                $promoCount++ ;
                            } endif; ?></div>
                </div>
            </section>

        <div id="testsection">

            <section class="block bg-primary text-center gap-p-md is-extended wow fadeInDown mb-3" id="filter-section" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <div class="block__b">
                    <div class="filter-holder ff-pp" >
                        <div class="form">
                            <form method="get" class="filter-form" id="shop-lisitng-frm" action="<?= $actionUrl; ?>">
                                <div class="row justify-content-center">
                                    <div class="col-sm-8 col-md-6 px-md-4">
                                        <div class="form-group form-group--lg position-relative dropdown-search ">
                                            <select name="shopID" class="js-shop-frm-input custom-select custom-select--lg custom-select--dark form-control select2-hidden-accessible border-white" id="filter-by-cat" tabindex="-1" aria-hidden="true" style="">
                                                <?php  $args =('post_type=shop&posts_per_page=-1&orderby=post_title&order=ASC&suppress_filters=true&ignore_sticky_posts=true&post_status=publish');
                                                $shopPost = get_posts($args);
                                               if( $shopPost ) :

                                                $selectedShop ="";
                                                ?>
                                                <option value="">Par enseignes</option>
                                                <?php foreach ($shopPost as $shop) {
                                                    if(!empty($inputShopID) & ($shop->ID==$inputShopID)) :
                                                        $selectedShop = 'selected';
                                                    endif;
                                                    ?>
                                                    <option value="<?=$shop->ID?>" <?=$selectedShop?>><?=$shop->post_title?></option>
                                                <?php }
                                                endif;
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex flex-wrap justify-content-center filter-row">
                                    <?php
                                    $c=1;
                                    $terms = get_terms( 'promo_cat', array(
                                        'hide_empty' => false,
                                    ) );
                                    $c=1;
                                    if(!empty($terms)):
                                        foreach($terms as $category):
                                            if(!empty($inputCategories) & in_array($category->term_id, $inputCategories)) :
                                                $checkedCat = 'checked="checked"';
                                            else:
                                                $checkedCat ='';
                                            endif;


                                            ?>
                                            <div class="col-6 col-sm-3 filter-col">
                                                <div class="form-group form-group--lg position-relative">
                                                    <input class="d-none filter-field js-shop-frm-input" id="filter-item0<?=$c?>" type="checkbox" name="cats[]"  value="<?=$category->term_id?>" <?=$checkedCat?> />
                                                    <label for="filter-item0<?=$c?>" class="border-fat input-label-btn"><span class="input-label-text"><?=$category->name?></span></label>
                                                </div>
                                            </div>
                                            <?php $c++; endforeach; endif;?>
                                </div>

                                <a href="<?php echo home_url(); ?>/services/promotion.php?pdfType=promotionList" class="btn simple text-white"><i class="icon icon-emails-interface-download-symbol"></i>TÉLECHARGER TOUS LES BONS </a>

                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>


            <!-- Reusable brand logo grid -->
            <section class="block block--grid last bg-faded text-center is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <div class="block__b">
                    <?php
                    $actionUrl = get_permalink();
                    $promoCount = $query->post_count;
                    $c=1;

                    //    var_dump($query);
                    if ($query->have_posts() ) :
                        while($query->have_posts()): $query->the_post();
                            $title = strip_tags(get_the_title());
                            $promoText = get_field('text');
                            $title2 = strip_tags(get_field('promotion_title_2'));
                            $permalink = get_permalink();
                            $logoTermID = get_the_terms($post->ID,'logo');
                            if(!empty($logoTermID[0]->term_id)) :
                                $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
                                $promoLogoImage = $promoLogoImage['url'];
                            endif;

                            $popUpContentPromotion = get_field('popup_contents');

                            $shopPermalink =   get_related_term_post_permalink($logoTermID[0]->term_id);
                            if($c==1) echo '<div class="row">';

                            ?>
                            <div class="col-sm-4 d-flex mb-3">
                                <div class="card card--brand has-floating-footer bg-white shadowed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                                    <div class="card-inner">
                                        <div class="meta-holder text-right">
                                            <?php app_social_share($permalink,$title,''); ?>
                                        </div>
                                        <div class="card__c mb-3">
                                            <figure class="brand-pic mt-3 mb-3"><a href="javascript:void(0);"><img alt="Brand Logo" class="img img-fluid" src="<?=$promoLogoImage?>" /></a></figure>
                                            <h2 class="h5 card__t ls-1 text-muted"><?php echo $title;?></h2>
                                            <p><?php echo $title2; ?></p>
                                        </div>
                                        <?php
                                        $modelID = 'promoModal'.$c;
                                        $ctaText = get_field('cta_text',$query->post->ID);
                                        $dateOfferValidation = get_field('date_offer_condition',$query->post->ID);
                                        //$modelID = 'promo-banner-modal-'.$promoCount;
                                        $bannerImage = get_field('image', $query->post->ID);
                                        $pdfGeneratorLink = home_url().'/services/promotion.php?id='.$query->post->ID;
                                        $title =$popUpContentPromotion['title'];
                                        $text = $popUpContentPromotion['text'];
                                        $popupLogo = $promoLogoImage;
                                        $popupDescription = $popUpContentPromotion['description'];
                                        $permalink = get_permalink();

                                        ob_start();
                                        //\App\promotion_layer_content($modelID,$shopPermalink,$promoLogoImage ,$ctaText ,$dateOfferValidation);
                                        \App\app_promotion_layer_content($modelID ,$bannerImage ,$pdfGeneratorLink ,$title ,$text ,$popupLogo ,$popupDescription ,$permalink, $link );
                                        $templateContent .= ob_get_clean();
                                        ?>

                                        <a href="" class="link-stacked" data-toggle="modal" data-target="#promoModal<?=$c?>"></a>

                                        <div class="card__f is-floated center-bottom">
                                            <button role="button" class="btn btn-mw btn-outline-primary border-fat d-block m-auto promo-btn js-promo-frm-btn" data-toggle="modal" data-target="#promoModal<?=$c?>">J’en profite</button>
                                            <?php if(!empty($shopPermalink)): ?>
                                                <div class="mt-4">
                                                    <a href="<?=$shopPermalink?>" class="text-muted ff-pp js-promo-frm-link"><i class="icon icon-plus"></i><u>Voir la boutique</u></a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $c++; if($c>$promoCount) echo '</div>'; ?>
                        <?php endwhile;
                        wp_reset_postdata();
                        //  \App\custom_pagination($query->max_num_pages,"",$paged, $actionUrl);
                        //wp_reset_query();
                        echo "<nav class='pagination-nav mt-2 mt-sm-3'><ul class='pagination justify-content-center'>";
                        \App\custom_pagination($query->max_num_pages,"",$paged, $actionUrl);
                        echo "</ul></nav>";
                    else :
                        ?>
                        <p><?php _e('Il n\'y a pas de nouvelles basées sur vos critères de sélection. Veuillez sélectionner d\'autres critères disponibles'); ?></p>
                    <?php endif; ?>
                </div><!-- /.Block body ends -->
            </section><!-- /.Reusable brand logo grid ends -->
            <!--to discard huge space on top remove gap-p-md class-->
            <?php echo $templateContent;  ?>

        </div><!-- /.Site container ends -->
    </div><!-- /.Site content ends -->
</main><!-- /.Site main content wrapper ends -->


<script>
    jQuery(document).ready(function () {
        var $shopListing = $('#shop-lisitng-frm ');
        $('#shop-lisitng-frm .js-shop-frm-input').change(function () {
            $shopListing.submit();
            localStorage.setItem('submitForm', 'true');

        });
    });

    jQuery( window ).on("load", function() {
        // Handler for .load() called.
        var checkFormSubmit = localStorage.getItem('submitForm');
        if(checkFormSubmit) {
            $('html, body').animate({
                scrollTop: $("#testsection").offset().top-70
            }, 1000);
            localStorage.removeItem('submitForm');
        }
    });

</script>

