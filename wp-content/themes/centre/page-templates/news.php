<?php
/* Template Name: News Page
 *
*/?>
<?php

$actionUrl = get_permalink();
//build query
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = [ 'post_type' => 'post', 'paged' => $paged,'posts_per_page' => 6,'orderby'=>'date','order'=>'DESC'];

$taxQuery = [];
$inputTerms = [];
$inputDate = '';

$inputCats = [];



if(!is_array($_GET['terms']) && isset($_GET['terms'])) {
    array_push($inputTerms,$_GET['terms']);
} else {
    if(isset($_GET['terms'])) $inputTerms = array_filter($_GET['terms'], 'is_numeric');
}


if(!is_array($_GET['cats']) && isset($_GET['cats'])) {
    array_push($inputCats,$_GET['cats']);
} else {
    if(isset($_GET['cats'])) $inputCats = array_filter($_GET['cats'], 'is_numeric');
}


if(!empty($inputTerms)){
    $taxQuery = [
        'taxonomy' => 'post_tag',
        'field'    => 'term_id',
        'terms'    => $inputTerms,
    ];
    $args['tax_query'] = [$taxQuery];
}


if(!empty($inputCats)){
    $taxQuery = [
        'taxonomy' => 'category',
        'field'    => 'term_id',
        'terms'    =>$inputCats,
    ];
    $args['tax_query'] = [$taxQuery];
}

//year
/*if(isset($_GET['y']) && $_GET['y'] != '') $inputDate = (int) $_GET['y'];

if(!empty($inputDate)){
    $args['date_query'] = [
        [
            'year' => $inputDate
        ]
    ];
}*/


if(isset($_GET['search']) && $_GET['search'] != '') $searchInput = $_GET['search'];

if($searchInput){
    $args['s'] = $searchInput;
}

$query = new WP_Query( $args );

?>
<main id="main" class="main" role="main">
    <div class="content">
        <div class="container position-static">

            <!-- Magazine/blog/news block -->
            <section class="block block--mag last pt-0 has-cover is-extended is-parallaxed wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/mag-bkg.jpg);">
                <div class="block__b">
                    <?php if(!\App\isRequestAjax()):?>
                    <div class="bg-inverse bg-sm-none is-xs-extended gap-p-md pb-3 pb-sm-4 pb-lg-6 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="filter-holder ff-pp">
                            <div class="form">
                                <form method="get" id="news-frm" action="<?= $actionUrl; ?>">
                                    <form method="get" class="filter-form" action="<?= $actionUrl; ?>">
                                        <div class="row justify-content-center">
                                            <div class="col-sm-8 col-md-6 px-md-4">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-group--lg position-relative">
                                                            <input type="text" name="search" class="form-control form-control--lg border-fat border-white" placeholder="Recherche par nom" />
                                                            <button type="submit" class="form__btn is-floated"><i class="icon icon-light icon-md icon-search mr-0"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-flex flex-wrap justify-content-center filter-row">
                                            <?php
                                            $c=1;
                                            $categories = get_categories( array(
                                                'orderby' => 'name',
                                                'order'   => 'ASC'
                                            ) );
                                            foreach($categories as $category):
                                                ?>
                                                <div class="col-6 col-sm-3 filter-col">
                                                    <div class="form-group form-group--lg position-relative">
                                                        <input <?=(in_array($category->term_id, $inputCats) ? 'checked="checked"' : '')?> type="checkbox" value="<?=$category->term_id?>" name="cats[]" class="d-none filter-field js-news-frm-input" id="filter-item0<?=$c?>" />
                                                        <label for="filter-item0<?=$c?>" class="border-fat input-label-btn text-inverse"><span class="input-label-text"><?=$category->name?></span></label>
                                                    </div>
                                                </div>
                                            <?php $c++; endforeach;?>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                        <script>
                            (function() {
                                window.onload = function() {
                                    var $newsForm = $('#news-frm');
                                    $('#news-frm .js-news-frm-input').change(function(){
                                        $newsForm.submit();
                                    });
                                };
                            })();
                        </script>
                    <?php endif; ?>
                    <div class="card-holder position-relative mt-4 mt-sm-0">

                            <?php
                            $postCount = $query->post_count;
                            $c=1;
                            $actionUrl = get_permalink();
                            //build query
                            if ($query->have_posts()):
                                while($query->have_posts()):$query->the_post();
                                    if($c==1) echo '<div class="row">';
                                    include __DIR__.'/../partials/news-item.php';
                                     $c++; if($c>$postCount) echo '</div>';
                                endwhile;
                              //  \App\custom_pagination($query->max_num_pages,"",$paged, $actionUrl);
                                //wp_reset_query();
                                echo "<nav class='pagination-nav mt-2 mt-sm-3 ". (($query->found_posts <= 6) ? 'd-none' : '')."'><ul class='pagination pagination--sm-white justify-content-center'>";
                                    \App\custom_pagination($query->max_num_pages,"",$paged, $actionUrl);
                                echo "</ul></nav>";

                            else :
                            ?>
                                <div class="text-white"><?php _e('Il n\'y a pas de nouvelles basées sur vos critères de sélection. Veuillez sélectionner d\'autres critères disponibles'); ?></div>
                        <?php endif; ?>
                </div><!-- /.Block body ends -->
            </section><!-- /.Magazine/blog/news block ends -->
        </div><!-- /.Site container ends -->
    </div><!-- /.Site content ends -->
</main><!-- /.Site main content wrapper ends -->

