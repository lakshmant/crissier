<?php
/* Template Name: Home Page
 *
*/?>
<main id="main" class="main" role="main">
    <div class="content">
        <?php
        get_template_part('partials/home/slider');
        ?>
        <div class="container position-static">
            <?php
            get_template_part('partials/home/promotion');
            get_template_part('partials/home/banner');
            get_template_part('partials/home/contest-newsletter');
            get_template_part('partials/home/news');
            get_template_part('partials/home/banner-after-news');
            ?>
        </div>
        <?php get_template_part('partials/home/social-feed'); ?>
    </div>
</main>