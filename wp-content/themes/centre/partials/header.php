<header id="masthead" class="topnavbar topnavbar--is-sticky wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" role="banner">
    <div class="collapsable-panel-group js-collapse">
        <div class="collapsable-panel bg-primary">
            <div class="container">
                <div class="form search-form">
                   <?php /* <form>
                        <div class="form-group mb-0 position-relative">
                            <input class="form-control form-control--lg border-fat border-white" type="text" placeholder="Rechercher sur le site" />
                            <button type="submit" class="form__btn is-floated"><i class="icon icon-light icon-md icon-search mr-0"></i></button>
                        </div>
                    </form> */ ?>
                   <?php echo get_search_form(); ?>
                </div>

            </div><!-- /.Site container ends -->
        </div><!-- /.Collapsable panel ends -->
    </div><!-- /.Collapsable panel group ends -->
    <div class="topnavbar__c">
        <div class="container">
            <div class="d-flex align-items-center topnavbar-row">
                <div class="topnavbar-row__i topnavbar-row__i--l">
                    <h1 class="m-0">
                        <a href="<?=home_url()?>" class="brand">
                            <figure class="brand__pic m-0 text-center"><img alt="Centre Commercial Crissier Logo" class="site-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"><figcaption class="brand-tagline text-uppercase text-white h6 hidden-sm-down">La vie côté shopping</figcaption></figure>
                        </a>
                    </h1>
                </div>
                <div class="topnavbar-row__i topnavbar-row__i--r">
                    <nav class="navbar flex justify-content-end">
                        <div class="navbar__header hidden-md-up">
                            <button class="navbar__toggle js-navbar-toggle" data-target="js-flyout-menu">
                                <span class="line-bar"></span>
                                <span class="line-bar"></span>
                                <span class="line-bar"></span>
                                <span class="line-bar"></span>
                            </button>
                        </div>
                        <div class="d-flex align-items-center justify-content-end">
                            <div class="js-flyout-menu menu-holder">
                                <?php wp_nav_menu( array( 'theme_location' => 'primary_menu','menu_class' => 'menu d-flex justify-content-end','container'=> '','container_class'=>'' ) ); ?>
                            </div>
                            <div class="actions d-flex align-items-center">
                                <div class="dropdown opening-hours-dropdown dropdown--lg position-static actions__i">
                                    <?php
                                    //$ouvertTitle = get_field('opening_status_title','options');
                                    $statusBannerTitle = get_field('status_banner_title','options');
                                    //$statusBannerTime = get_field('status_banner_time','options');
                                    $commerceAlimentairesTitle= get_field('commerces_alimentaires_title','options');
                                    $commercesAlimentaires = get_field('commerces_alimentaires','options');
                                    $commerceNonAlimentairesTitle= get_field('commerces_non-alimentaires_title','options');
                                    $commercesNonAlimentaires = get_field('commerces_non-alimentaires','options');
                                    $restaurationTitle= get_field('restauration_title','options');
                                    $restauration = get_field('restauration','options');
                                    ?>

                                    <button role="button" class="dropdown-toggle dropdown__toggle--has-icon actions__dropdown-toggle" id="openingHoursDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon icon-md icon-clock"></i><span class="hidden-sm-down js-label-shop-status">Ouvert</span></button>
                                    <ul class="dropdown-menu bg-primary" aria-labelledby="openingHoursDropdown">
                                        <li class="dropdown-item">
                                            <div class="dropdown-item-c opening-hours text-white ff-os">
                                                <div class="opening-hours__h text-center">
                                                    <i class="icon icon-clock icon-light icon-xl mr-0 mb-3"></i>
                                                    <div class="h5 ff-pp"><?=$statusBannerTitle?></div>
                                                    <div class="h1 ff-pp fw-b"><strong class=" js-shop-close-time"></strong></div>
                                                </div>
                                                <div class="opening-hours__f mt-6">
                                                    <div class="row">
                                                        <div class="col-sm-6 col-md-4 mb-3 mb-sm-0">
                                                            <h4 class="h5"><?=$commerceAlimentairesTitle?></h4>
                                                            <ul class="lists lists--default mt-3">
                                                                <?php foreach ($commercesAlimentaires as $ca) :?>
                                                                <li class="lists__i row"><span class="col-6"><strong><?=$ca['day']?></strong></span><span class="col-6"><?=$ca['time']?></span></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4 mb-3 mb-sm-0">
                                                            <h4 class="h5"><?=$commerceNonAlimentairesTitle?></h4>
                                                            <ul class="lists lists--default mt-3">
                                                                <?php foreach ($commercesNonAlimentaires as $ca) :?>
                                                                <li class="lists__i row"><span class="col-6"><strong><?=$ca['day']?></strong></span><span class="col-6"><?=$ca['time']?></span></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4 mb-3 mb-sm-0">
                                                            <h4 class="h5">&nbsp;<?=$restaurationTitle?></h4>
                                                            <ul class="lists lists--default mt-3">
                                                                <?php
                                                                foreach ($restauration as $restaurat) :
                                                                ?>
                                                                <li class="lists__i row"><span class="col-6"><strong><?=$restaurat['day']?></strong></span><span class="col-6"><?=$restaurat['time']?></span></li>
                                                                <?php endforeach;?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <ul class="actions-list d-flex actions__i">
                                    <li class="actions-list__i"><a href="<?=get_field('google_map_link','options')?>" target="_blank"><i class="icon icon-md icon-pin mr-0"></i></a></li>
                                    <li class="actions-list__i"><a href="tel:<?=get_field('telephone_number','options')?>"><i class="icon icon-md icon-phone mr-0"></i></a></li>
                                    <li class="actions-list__i d-none"><a href="" class="js-toggle-panel" data-target="collapsable-panel-group"><i class="icon icon-md icon-search mr-0"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div><!-- /.Main container ends -->
    </div><!-- /.Site header content ends -->
</header><!-- /.Site header ends -->

