
<div class="col-sm-4 mb-3">
    <div class="card card--sq has-promo-tag bg-white shadowed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <?php
        $logoTermID = get_the_terms($post->ID,'logo');
        if(!empty($logoTermID[0]->term_id)) :
        $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
        endif;
        ?>
        <?php if(!empty($promoLogoImage)) : ?>
            <div class="card__c center">
                <figure class="card__pic mb-0"><img alt="Instagram Image" class="img img-fluid" src="<?=$promoLogoImage['url']?>" /></figure>
            </div>
        <?php endif; ?>
        <?php
        $promoActivate = get_field('activate_promo',$post->ID);
      //  echo 'I am pr'.$value;
        if($promoActivate[0]=='yes') :
            ?>
            <div class="promo-tag-holder text-white">
                <i class="icon icon-price-tag icon-white mb-2 mr-0"></i>
                <div class="text-uppercase ff-pp fw-l ls-1">Promo</div>
            </div>
        <?php endif;?>
        <a href="<?php the_permalink(); ?>" class="link-stacked"></a>
    </div>
</div>