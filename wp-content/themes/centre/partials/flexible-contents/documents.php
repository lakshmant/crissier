<?php
$docGroup = get_sub_field('document_group');

$title = !empty($docGroup['title'])?$docGroup['title']:'';
$bgColor = !empty($docGroup['background_color'])?$docGroup['background_color']:'white';
$pdfType = !empty($docGroup['pdf_type'])?$docGroup['pdf_type']:'';
$docFile = get_sub_field('documents');
?>

<section class="block block--demo gap-p-eq bg-<?=$background?> is-extended wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeIn;">
    <header class="block__h text-center">
        <h2 class="mb-3 mb-sm-5"><?=$title?></h2>
    </header>
    <div class="block__b">
        <!-- Action button utilities -->
        <div class="button-utilities">
            <ul class="row button-utilities__list" role="list">

                <?php foreach ($docFile as $file) :
                        $docLink = $file['document'];
                        $docTitle = $file['title'];
                    if(!empty($docLink)) :
                    ?>
                    <li class="col-sm-6 mb-3 mb-sm-4">
                        <a href="<?=$docLink?>" class="button-utilities__link"><i class="button-picto button-picto--<?=$pdfType?>"></i><span><strong><?=$docTitle?></strong></span></a>
                    </li>
                <?php endif; endforeach; ?>


            </ul>
        </div><!-- /.Action button utilities ends -->
    </div>
</section>