<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 10/11/2017
 * Time: 2:35 PM
 */

$heroOptions = get_sub_field('hero_column_group');
$bgColor = (isset($heroOptions['background_color'])) ? $heroOptions['background_color'] : 'white';
$slider = get_sub_field('slider');
$uploadSlider = get_sub_field('upload_slider');
?>
<?php $content = get_sub_field('content'); ?>
<!-- Reusable simple text block -->
<section class="block block--text gap-p-eq bg-<?=$bgColor?> is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="row align-items-center">
            <div class="col-sm-6 mb-3 mb-sm-0">

                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <?php if($uploadSlider!='yes') :
                        $photo = get_sub_field('image');
                        /*if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_MEDIUM);*/
                        $photo = !empty($photo)? $photo : "";
                        ?>
                        <figure class="card__pic mb-0"><img alt="Post Thumbnail" class="img img-xs-full img-fluid" src="<?=$photo?>" /></figure>
                    <?php endif; ?>
                    <?php if($uploadSlider=='yes') : ?>
                        <div class="swiper-container slider slider--posts js-slider-hero">
                            <div class="swiper-wrapper slider__wrapper">

                                <?php $sliderImages = get_sub_field('slider');
                                foreach ($sliderImages as $image) :
                                    /*  if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($image['image']), \App\IMAGE_SIZE_MEDIUM);*/
                                    $photo = !empty($image['image'])? $image['image'] : "";

                                    ?>
                                    <div class="swiper-slide slider__slide">
                                        <div class="slider__b">
                                            <figure class="slider__pic mb-0 text-center"><img alt="Hero Slider Image" class="img img-xs-full" src="<?=$photo?>" /></figure>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <!-- If we need pagination -->
                            <div class="swiper-pagination d-none"></div>

                            <!-- Slider controls -->
                            <div class="swiper-button swiper-button-next"><i class="icon icon-right-chev icon-faded mr-0"></i></div>
                            <div class="swiper-button swiper-button-prev"><i class="icon icon-left-chev icon-faded mr-0"></i></div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-6 <?=($heroOptions['type']=='image-text')? 'flex-sm-first': ''?>">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__b pl-0 pr-sm-3 py-0">
                        <?=$content?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Reusable simple text block ends -->