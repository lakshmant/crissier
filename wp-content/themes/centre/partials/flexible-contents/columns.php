<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 10/11/2017
 * Time: 3:33 PM
 */

$colGroup = get_sub_field('columns_group');

$title = $colGroup['title'];
$text = $colGroup['text'];
$bgColor = $colGroup['background_color'];
$allignment = $colGroup['allignment'];
$colContents = get_sub_field('columnns');
$colCount = count($colContents);

if($colCount==1) :

?>
            <!-- Reusable simple text block -->
            <section class="block block--text gap-p-eq bg-<?=$bgColor?> <?=$allignment?> is-extended  wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <div class="text-container--sm">
                    <?php if($title): ?>
                        <header class="block__h"><h2><?=$title?></h2></header>
                    <?php endif; ?>
                    <?php if(!empty($text)) : ?>
                        <p><?=$text?></p>
                    <?php endif; ?>
                    <?php
                    if($colContents[0]['content']) : ?>
                    <div class="block__b">
                    <?php echo $colContents[0]['content']; ?>
                    </div><!-- /.Block body ends -->
                    <?php endif; ?>
                </div>
            </section><!-- /.Reusable simple text block ends -->
<?php elseif($colCount==2 || $colCount==3):

    if($colCount==2) :
  /*  if($image) :
        $headerClass = $allignment." mb-4 mb-lg-5";
    else :*/
    $headerClass = $allignment."  mb-4 mb-lg-5";
    $contentWrapperClass = "col-sm-6";
    $contentWrapClass = "bg-".$bgColor;
else :
    $headerClass = $allignment."mb-3";
    $contentWrapperClass = "col-sm-4";
    $contentWrapClass = "p-0 pl-sm-2";
endif;
    ?>

     <section class="block block--text gap-p-eq bg-<?=$bgColor?> is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
         <header class="block__h <?=$headerClass?>">
             <h2 class="mb-0"><?=$title?></h2>
             <?php if(!empty($text)) : ?>
                 <p><?=$text?></p>
             <?php endif; ?>
        </header>
         <div class="block__b">
             <div class="row">
                 <?php

                 // check if the repeater field has rows of data
                 if( have_rows('columnns') ):
                     // loop through the rows of data
                     while ( have_rows('columnns') ) : the_row();
                         $image = get_sub_field('image');
                         $title = get_sub_field('title');
                         $content = get_sub_field('content');

                     /*    if($image) $image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_MEDIUM);*/
                             // $headerClass = $allignment." mb-4 mb-lg-5";
                         if(empty($image)):
                             $paddingClass = "p-0";
                         else :
                             $paddingClass = "";
                         endif;
                         ?>

                         <div class="<?=$contentWrapperClass?> mb-3 mb-sm-0">
                             <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                                 <?php if(!empty($image)) : ?>
                                     <figure class="card__pic mb-0"><img alt="Post Thumbnail" class="img img-xs-full img-fluid" src="<?=$image?>" /></figure>
                                 <?php endif; ?>
                                 <!-- Add bg-white to make post body white -->
                                 <div class="card__b <?=$contentWrapClass?> <?=$paddingClass?>">
                                     <?php $title  = !empty($title) ? $title :'&nbsp;'; ?>
                                         <h3 class="text-primary"><?=$title?></h3>
                                     <?php
                                     if(!empty($content)) :
                                         echo $content;
                                     endif;
                                     ?>
                                 </div>
                             </div>
                         </div>
                     <?php endwhile; endif; ?>

             </div>
         </div><!-- /.Block body ends -->
     </section><!-- /.Reusable simple text block ends-->
<?php endif;