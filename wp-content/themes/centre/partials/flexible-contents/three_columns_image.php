<?php
$images = get_sub_field('images');
$bgColor = get_sub_field('background_color');
?>
<section class="block block--grid gap-p-eq bg-<?=$bgColor?> text-center is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="row">

            <?php foreach ($images as $image) :
                    $imageLink = !empty($image['link']) ? $image['link'] : '' ;
                    $image =  !empty($image['image']) ? $image['image'] : '' ;  
                ?>
            <div class="col-sm-4 mb-3">
                <div class="card card--sq bg-white shadowed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c">
                        <!-- Later image will be added in inner pages so we need to remove "d-none" class from below image container -->
                        <figure class="card__pic d-none zoom-effect-holder mb-0"><img alt="Instagram Image" class="img zoom-effect img-fluid" src="<?=$image?>" /></figure>
                    </div>
                    <a href="<?=$imageLink?>" class="link-stacked"></a>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Reusable brand logo grid ends -->