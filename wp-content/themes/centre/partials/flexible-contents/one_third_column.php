<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 1/24/2018
 * Time: 2:59 PM
 */

$title = get_sub_field('title');
$text = get_sub_field('text');
$bgColor = get_sub_field('background_color');
?>
<!-- Reusable simple text block -->
<section class="block block--text gap-p-eq bg-<?=$bgColor?> is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="row">
            <div class="col-sm-4 mb-3 mb-sm-0">
                <div class="card pr-5 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <?php $title  = !empty($title) ? $title :'&nbsp;'; ?>
                    <h2 class="mb-0"><?=$title?></h2>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <?php echo !empty($text) ? $text :''; ?>
                </div>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Reusable simple text block ends -->
