<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 1/23/2018
 * Time: 5:52 PM
 */
$videoType = get_sub_field('type');
$youtube = get_sub_field('video_url');
preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#", $youtube, $matches);
$youtubeVideoID = $matches[0][0];

if($videoType=='non-background') :
?>
<!-- Reusable simple text block -->
<?php if(!empty($youtubeVideoID)) : ?>
    <section class="block block--text gap-p-eq bg-faded is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <div class="body__b">
            <div class="video-holder">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?=$youtubeVideoID?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section><!-- /.Reusable simple text block ends -->
<?php endif; ?>

<?php else :

    $title = get_sub_field('title');
    $image = get_sub_field('background_image');
/*    $image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_CMS_SLIDER);*/
    ?>

<!-- Reusable simple text block -->
<section class="promo promo--text position-relative is-extended-full wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <figure class="promo__pic mb-0"><img alt="Video Thumbnail" class="img img-full img-fluid" src="<?=$image?>" /></figure>
    <div class="promo__c is-floated center zindex-2 text-center">
        <?php if(!empty($title)) : ?>
            <div class="h2 text-white"><?=$title?></div>
    <?php endif; ?>
        <div class="lightbox">
            <a href="https://www.youtube.com/embed/<?=$youtubeVideoID?>?rel=0&amp;controls=0&amp;showinfo=0" class="lightbox__play js-video-play"><i class="icon icon-play-round mr-0"></i></a>
        </div>
    </div>
</section><!-- /.Reusable simple text block ends -->
<?php endif;