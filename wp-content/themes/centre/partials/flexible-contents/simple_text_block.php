<!-- Reusable simple text block -->
<?php $title = get_sub_field('title');
$text = get_sub_field('text');
?>

<section class="block block--quote gap-p-eq bg-primary text-white is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--sm">
        <header class="block__h">
            <?php if(!empty($title)) : ?>
                <h2 class="mb-0"><?=$title?></h2>
            <?php endif; ?>
           <?php if(!empty($text))echo $text; ?>
        </header>
    </div>
</section><!-- /.Reusable simple text block ends -->