<?php
$promotion = get_sub_field('promotion_display');
$bgColor = !empty(get_sub_field('background_color')) ? get_sub_field('background_color') : 'white';
?>
<section class="block block--grid gap-p-eq bg-<?=$bgColor?> text-center is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeInDown;">
    <div class="block__b">

        <div class="row">

            <?php
           // var_dump($promotion);
            foreach ($promotion as $promoID) {
                $title1 = strip_tags(get_the_title($promoID));
                $promoText = get_field('text',$promoID);
                $title2 = strip_tags(get_field('promotion_title_2',$promoID));
                $permalink = get_the_permalink($promoID);

                $logoTermID = get_the_terms($promoID, 'logo');
                if (!empty($logoTermID[0]->term_id)) :
                    $promoLogoImage = get_field('logo_image', 'logo_' . $logoTermID[0]->term_id);
                    $promoLogoImage = $promoLogoImage['url'];
                endif;
            ?>

            <div class="col-sm-4 d-sm-flex mb-3 mb-sm-0">
                <div class="card card--brand has-floating-footer bg-white shadowed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeInDown;">
                    <div class="card-inner">
                        <div class="meta-holder text-right">
                            <?php app_social_share($permalink,$title1,''); ?>
                        </div>
                        <div class="card__c mb-3">
                            <figure class="brand-pic mt-3 mb-3"><a href=""><img alt="Brand Logo" class="img img-fluid" src="<?=$promoLogoImage?>"></a></figure>
                            <h2 class="h5 card__t ls-1"><a href="" class="text-muted"><?=$title1?></a></h2>
                            <p><?=$promoText?></p>
                        </div>
                        <div class="card__f is-floated center-bottom js-promo-frm-btn-wrap">
                          <!--  <a href="" class="btn btn-mw btn-outline-primary border-fat d-block m-auto">J’en profite</a>-->
                            <a href="" id="promo-btn" class="btn btn-mw btn-outline-primary border-fat d-block m-auto promo-btn js-promo-frm-btn" data-id="<?=$promoID?>" data-toggle="modal" data-target="#promoModel">J’en profite</a>
                            <?php
                                $shopPermalink =   get_related_term_post_permalink($logoTermID[0]->term_id);
                                if(!empty($shopPermalink)):
                                ?>
                                <div class="mt-4">
                                    <a href="<?php echo $shopPermalink; ?>" class="text-muted ff-pp  js-promo-frm-link"><i class="icon icon-plus"></i><u>Voir la boutique</u></a>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div><!-- /.Block body ends -->
</section>

<!-- Newsletter modal -->
<div class="modal modal--sm fade text-white text-center" id="promoModel" tabindex="-1" role="dialog" aria-labelledby="newsletterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content has-texture bg-primary">
            <button class="dismiss-modal" data-dismiss="modal" aria-label="Close"><i class="icon icon-close mr-0"></i></button>
            <div class="modal-body">
                <i class="icon icon-newsletter icon-xl icon-light mb-3 mr-0"></i>
                <div class="h2 mb-2">restez informé Promotion</div>
                <div class="ff-os fw-l">Soyez à l’affût de tous les bons plans, abonnez-vous !</div>
                <div class="form mt-4">
                    <?php
                    echo do_shortcode('[contact-form-7 id="421" title="Promotion Form" html_class="subscribe-form"]')
                    ?>
                    <div class="para ff-os text-secondary"><small>Vous pouvez vous désabonner en tout temps</small></div>
                </div>
                <div class="mt-4"><a href="" target="_blank" class="text-white js-promo-frm-link-2" ><i class="icon icon-plus icon-light"></i><u>Non merci</u></a></div>
            </div>
        </div>
    </div>
</div><!-- /.Newsletter modal ends -->

<script>
    jQuery(".js-promo-frm-btn").click(function(){
        var promoID = $(this).attr('data-id');
        $("#promoID").val(promoID);
        var $promoLink = $(this).parent().find('.js-promo-frm-link');
        if($promoLink && $promoLink.attr('href')){
            $('#promoModel .js-promo-frm-link-2').attr('href', $promoLink.attr('href'));
        }
    });
</script>