<!-- Reusable simple text block -->
<section class="block block--text gap-p-sm bg-primary text-white is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">

        <div class="row align-items-center">
            <div class="col-sm-4 mb-4 mb-sm-0">
                <div class="card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <figure class="card__pic zoom-effect-holder mb-0">
                        <?php
                        $image = get_sub_field('image');
                        if($image) $image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_QUOTE);
                        ?>
                        <img alt="Post thumbnail" class="img img-full fluid-img zoom-effect" src="<?=$image?>" />
                    </figure>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="card pl-sm-3 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <blockquote class="text-justify mb-0" cite="">
                        <p>“ <?=get_sub_field('quote')?>”</p>
                    </blockquote>
                </div>
            </div>
        </div>

    </div><!-- /.Blcok body ends -->
</section><!-- /.Reusable simple text block ends -->