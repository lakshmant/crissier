<?php
$contactForm = get_sub_field('contact_shortcode');

if(!empty($contactForm)):
?>

<section class="block block--text gap-p-eq bg-white is-extended text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="text-container--xs">
        <header class="block__h">
            <?php $formTitle = get_sub_field('form_title');
            $formText = get_sub_field('form_text');
            if(!empty($formTitle)):
            ?>
            <h2 class="mb-0"><?=$formTitle?></h2>
            <?php endif; ?>
            <p><?=$formText?></p>
        </header>
        <div class="block__b">
            <div class="form mt-4 mt-sm-5 px-0 px-md-12 px-lg-5">
                <?php echo do_shortcode('[contact-form-7 id="217" title="Contact form 1" html_class="cms-form"]'); ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!-- /.Reusable simple text block ends -->
