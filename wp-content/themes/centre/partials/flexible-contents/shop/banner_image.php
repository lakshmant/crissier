<!-- Reusable simple text block -->

<?php
$photo = get_sub_field('image');
if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_SHOP_FLEXIBLE_BANNER);
$photo = !empty($photo)? $photo : '';

$bgColor = !empty(get_sub_field('background_color')) ? get_sub_field('background_color') : 'white';

?>
<section class="block block--text bg-<?=$bgColor?> gap-p-eq is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <figure class="zoom-effect-holder mb-0">
            <img alt="Shop Image" class="img img-fluid zoom-effect" src="<?=$photo?>" />
        </figure>
    </div><!-- /.Block body ends -->
</section><!-- /.Reusable simple text block ends -->