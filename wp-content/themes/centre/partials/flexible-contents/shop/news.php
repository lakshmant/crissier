<!-- Blog/news block -->
<?php
    $title = !empty(get_sub_field('title'))?get_sub_field('title'): '';
    $text = !empty(get_sub_field('text'))?get_sub_field('text'):'';
    $bgColor = !empty(get_sub_field('background_color')) ? get_sub_field('background_color') : 'white';

?>

<section class="block block--grid bg-<?=$bgColor?> gap-p-md is-extended wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <header class="block__h wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <div class="h5 text-muted text-uppercase ls-1 mb-2 mb-sm-3"><?=$title?></div>
        <div class="h1"><?=$text?></div>
    </header>
    <div class="block__b">
        <div class="card-holder position-relative mt-4 mt-sm-6">
            <div class="row">

                <?php
                $categories = get_the_terms($post->ID,'category');
                $catSlug = $categories[0]->slug;
                $termID = $categories[0]->term_id;
                $query = new WP_Query( array(  'post_type' => 'post',posts_per_page =>3 ,'cat' => $termID ) );
                while($query->have_posts()): $query->the_post();
                    $imgSrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );
                    $largeImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($imgSrc[0]), \App\IMAGE_SIZE_NEWS_LARGE);
                ?>

                <div class="col-sm-4 d-sm-flex mb-3">
                    <article class="article-card article-card--sm shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?=$largeImage?>" /></figure>
                            <div class="article-card__c">
                                <div class="h5 promo-cat text-muted"><?=$catSlug?></div>
                                <h2 class="promo-title"><?php the_title(); ?></h2>
                                <div class="promo-tags ff-os"><?php App\kd_related_terms($post->ID); ?></div>
                            </div>
                            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <i class="icon icon-light icon-plus card-icon mr-0"></i>
                            </div>
                        </div>
                        <a href="<?=the_permalink()?>" class="link-stacked"></a>
                    </article>
                </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
            <div class="text-holder text-center mt-2 mt-sm-3">
                <a href="<?=home_url('/news')?>" class="text-muted ff-pp"><i class="icon icon-plus"></i><u>voir tout les articles</u></a>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Blog/news block ends -->
