<?php
$title = !empty(get_sub_field('title'))?get_sub_field('title'): '';
$text = !empty(get_sub_field('text'))?get_sub_field('text'):'';
$bgColor = !empty(get_sub_field('background_color')) ? get_sub_field('background_color') : 'white';

?>

<section class="block block--grid pt-0 gap-p-eq bg-<?=$bgColor?> last is-extended wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <header class="block__h wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <div class="h5 text-muted text-uppercase ls-1 mb-2 mb-sm-3"><?=$title?></div>
        <div class="h1"><?=$text?></div>
    </header>
    <div class="block__b">
        <div class="card-holder position-relative mt-4 mt-sm-6">
            <div class="swiper-container carousel js-carousel">
                <div class="swiper-wrapper">

                    <?php
                    $categories = get_the_terms($post->ID,'category');
                    $termID = $categories[0]->term_id;;
                    $args = array(
                        'post_type' => 'shop',
                        'post__not_in' => array($post->ID),
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'term_id',
                                'terms'    => $termID
                            ),
                        ),
                    );
                    $query = new WP_Query( $args );
                    while ($query->have_posts()): $query->the_post();
                        $logoTermID = get_the_terms($post->ID,'logo');
                        if(!empty($logoTermID[0]->term_id)) :
                            $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
                        endif;
                    ?>
                        <div class="swiper-slide carousel__slide">
                            <div class="card card--sq bg-white shadowed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                                <div class="card__c center">
                                    <figure class="card__pic mb-0"><img alt="Instagram Image" class="img img-fluid" src="<?=$promoLogoImage['url']?>" /></figure>
                                </div>
                                <a href="<?php the_permalink(); ?>" class="link-stacked"></a>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_postdata();?>

                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination hidden-xs-down"></div>
                <!-- Slider controls -->
                <div class="swiper-button swiper-button-next shape-sq hidden-sm-up"><i class="icon icon-right-chev icon-primary icon-lg mr-0"></i></div>
                <div class="swiper-button swiper-button-prev shape-sq hidden-sm-up"><i class="icon icon-left-chev icon-primary icon-lg mr-0"></i></div>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Blog/news block ends -->
