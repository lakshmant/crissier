<?php
$logoTermID = get_the_terms($post->ID,'logo');
if(!empty($logoTermID[0]->term_id)) :
    $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
endif;

$shopPageLink = get_permalink($post->ID);

$args = array(
    'post_type' => 'promotions',
    'tax_query' => array(
        array(
            'taxonomy' => 'logo',
            'field'    => 'term_id',
            'terms'    => $logoTermID[0]->term_id
        ),
    ),
    'posts_per_page' => 1
);

$query = new WP_Query( $args );
while ($query->have_posts()): $query->the_post();
?>
<section class="promo promo--text bg-primary text-center gap-p-eq is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="promo__c promo__c--md">
        <div class="row justify-content-end align-items-center">
            <div class="col-sm-12 col-lg-11 px-3">
                <div class="card text-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                    <figure class="card__pic mb-0 mb-sm-3 mb-md-4"><a href=""><img alt="Brand Logo" class="img img-fluid" src="<?=$promoLogoImage['url']?>" /></a></figure>
                    <div class="h1 fw-r mb-2 pb-1">
                        <?php $title = strip_tags(get_the_title());
                            echo $title;
                        ?>
                    </div>
                    <?php $text = get_field('text');?>
                    <p><?=$text?></p>
                    <p>&nbsp;</p>
                    <a href="javascript:void(0);" class="btn btn-mw btn-outline-faded border-fat" role="button" data-id="<?=$query->post->ID?>" data-toggle="modal" data-target="#promoModal">J’en profite</a>
                    <!-- <p>&nbsp;</p>
                    <a href="<?php //the_permalink();?>" class="text-secondary ff-pp"><i class="icon icon-plus"></i><u>voir la fiche de la boutique</u></a> -->
                </div>
            </div>
        </div>
    </div><!-- /.Prom content ends -->
</section><!-- /.Reusable promotion block ends -->
   <?php
//    $rightLayerText1 = get_field("cta_text" );
  //  $rightLayerText2 = get_field("date_offer_condition");
    $logoTermID = get_the_terms($query->post->ID,'logo');
    if(!empty($logoTermID[0]->term_id)) :
        $popupLogo = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
        $popupLogo = $promoLogoImage['url'];
    endif;

    $bannerImage = get_field('image', $query->post->ID);
    $popUpContentPromotion = get_field('popup_contents',$query->post->ID);

    $pdfGeneratorLink = home_url().'/services/promotion.php?id='.$query->post->ID;
    $title =$popUpContentPromotion['title'];
    $text = $popUpContentPromotion['text'];
    $popupDescription = $popUpContentPromotion['description'];
    $link = $popUpContentPromotion['link'];

    \App\app_promotion_layer_content('promoModal' ,$bannerImage ,$pdfGeneratorLink ,$title ,$text ,$popupLogo ,$popupDescription ,$link );

    // \App\promotion_layer_content('promoModal',$shopPageLink,$promoLogoImage['url'], $rightLayerText1,$rightLayerText2);
    ?>
<?php endwhile; wp_reset_postdata();