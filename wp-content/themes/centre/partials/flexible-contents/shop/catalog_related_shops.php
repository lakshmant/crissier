<!-- Blog/news block -->
<?php

$title = !empty(get_sub_field('catalog_title'))?get_sub_field('catalog_title'): '';
$text = !empty(get_sub_field('catalog_text'))?get_sub_field('catalog_text'):'';
$bgColor = !empty(get_sub_field('background_color')) ? get_sub_field('background_color') : 'white';
?>

<section class="block block--grid bg-<?=$bgColor?> gap-p-md is-extended wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <header class="block__h wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <div class="row no-gutters justify-content-end">
            <div class="col-sm-8">
                <div class="text-card text-card--xl js-text-card-pushed-top has-arrow down left-bottom white bg-white mt-offset-eq">
                    <div class="h1 mb-2 pb-2"><?=$title?></div>
                    <p class="ff-os"><?=$text?></p>
                </div>
            </div>
        </div>
    </header><!-- /.Block header ends -->
    <div class="block__b">
        <div class="card-holder position-relative pt-0 pt-md-3 mt-6 mt-sm-7">
            <div class="row">
                <div class="col-sm-4 d-sm-flex mb-3">
                    <article class="article-card article-card--md shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo get_template_directory_uri(); ?>/contents/mag-lg-img01.jpg" /></figure>
                            <div class="article-card__c">
                                <ul class="promo-action-list is-floated right d-inline-block">
                                    <li class="mb-2"><a href=""><i class="icon icon-share icon-md icon-faded mr-0"></i></a></li>
                                    <li><a href=""><i class="icon icon-heart-outline icon-md icon-faded mr-0"></i></a></li>
                                </ul>
                                <h2 class="promo-title pr-0 pr-sm-3 mb-3 mb-sm-4"><span class="text-dark">veste blanche piquée BEA</h2>
                                <div class="promo-price h5 mb-0 ls-1">139.99-</div>
                            </div>
                            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <i class="icon icon-light icon-plus card-icon mr-0"></i>
                            </div>
                        </div>
                        <a href="" class="link-stacked"></a>
                    </article>
                </div>
                <div class="col-sm-4 d-flex mb-3">
                    <article class="article-card article-card--md shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo get_template_directory_uri(); ?>/contents/mag-sm-img01.jpg" /></figure>
                            <div class="article-card__c">
                                <ul class="promo-action-list is-floated right d-inline-block">
                                    <li class="mb-2"><a href=""><i class="icon icon-share icon-md icon-faded mr-0"></i></a></li>
                                    <li><a href=""><i class="icon icon-heart-outline icon-md icon-faded mr-0"></i></a></li>
                                </ul>
                                <h2 class="promo-title pr-0 pr-sm-3 mb-3 mb-sm-4"><span class="text-dark">veste blanche piquée BEA</h2>
                                <div class="promo-price h5 mb-0 ls-1">139.99-</div>
                            </div>
                            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <i class="icon icon-light icon-plus card-icon mr-0"></i>
                            </div>
                        </div>
                        <a href="" class="link-stacked"></a>
                    </article>
                </div>
                <div class="col-sm-4 d-flex mb-3">
                    <article class="article-card article-card--md shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo get_template_directory_uri(); ?>/contents/mag-sm-img02.jpg" /></figure>
                            <div class="article-card__c">
                                <ul class="promo-action-list is-floated right d-inline-block">
                                    <li class="mb-2"><a href=""><i class="icon icon-share icon-md icon-faded mr-0"></i></a></li>
                                    <li><a href=""><i class="icon icon-heart-outline icon-md icon-faded mr-0"></i></a></li>
                                </ul>
                                <h2 class="promo-title pr-0 pr-sm-3 mb-3 mb-sm-4"><span class="text-dark">veste blanche piquée BEA</h2>
                                <div class="promo-price h5 mb-0 ls-1">139.99-</div>
                            </div>
                            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <div class="shape-sq picto-holder  bg-primary d-inline-block">
                                    <i class="icon icon-light icon-plus card-icon mr-0"></i>
                                </div>
                            </div>
                            <a href="" class="link-stacked"></a>
                    </article>
                </div>
                <div class="col-sm-4 d-flex mb-3">
                    <article class="article-card article-card--md shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo get_template_directory_uri(); ?>/contents/mag-lg-img01.jpg" /></figure>
                            <div class="article-card__c">
                                <ul class="promo-action-list is-floated right d-inline-block">
                                    <li class="mb-2"><a href=""><i class="icon icon-share icon-md icon-faded mr-0"></i></a></li>
                                    <li><a href=""><i class="icon icon-heart-outline icon-md icon-faded mr-0"></i></a></li>
                                </ul>
                                <h2 class="promo-title pr-0 pr-sm-3 mb-3 mb-sm-4"><span class="text-dark">veste blanche piquée BEA</h2>
                                <div class="promo-price h5 mb-0 ls-1">139.99-</div>
                            </div>
                            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <i class="icon icon-light icon-plus card-icon mr-0"></i>
                            </div>
                        </div>
                        <a href="" class="link-stacked"></a>
                    </article>
                </div>
                <div class="col-sm-4 d-flex mb-3">
                    <article class="article-card article-card--md shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo get_template_directory_uri(); ?>/contents/mag-sm-img01.jpg" /></figure>
                            <div class="article-card__c">
                                <ul class="promo-action-list is-floated right d-inline-block">
                                    <li class="mb-2"><a href=""><i class="icon icon-share icon-md icon-faded mr-0"></i></a></li>
                                    <li><a href=""><i class="icon icon-heart-outline icon-md icon-faded mr-0"></i></a></li>
                                </ul>
                                <h2 class="promo-title pr-0 pr-sm-3 mb-3 mb-sm-4"><span class="text-dark">veste blanche piquée BEA</h2>
                                <div class="promo-price h5 mb-0 ls-1">139.99-</div>
                            </div>
                            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <i class="icon icon-light icon-plus card-icon mr-0"></i>
                            </div>
                        </div>
                        <a href="" class="link-stacked"></a>
                    </article>
                </div>
                <div class="col-sm-4 d-flex mb-3">
                    <article class="article-card article-card--md shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo get_template_directory_uri(); ?>/contents/mag-sm-img02.jpg" /></figure>
                            <div class="article-card__c">
                                <ul class="promo-action-list is-floated right d-inline-block">
                                    <li class="mb-2"><a href=""><i class="icon icon-share icon-md icon-faded mr-0"></i></a></li>
                                    <li><a href=""><i class="icon icon-heart-outline icon-md icon-faded mr-0"></i></a></li>
                                </ul>
                                <h2 class="promo-title pr-0 pr-sm-3 mb-3 mb-sm-4"><span class="text-dark">veste blanche piquée BEA</h2>
                                <div class="promo-price h5 mb-0 ls-1">139.99-</div>
                            </div>
                            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <div class="shape-sq picto-holder  bg-primary d-inline-block">
                                    <i class="icon icon-light icon-plus card-icon mr-0"></i>
                                </div>
                            </div>
                            <a href="" class="link-stacked"></a>
                    </article>
                </div>
            </div>
            <div class="text-holder text-center mt-2 mt-md-3">
                <a href="" class="text-muted ff-pp"><i class="icon icon-plus"></i><u>voir tout les articles</u></a>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Blog/news block ends -->

<div class="gap-p-xs pb-0 hidden-xs-down"></div>

<script>
    ;(function($) {
        addSpaceOnPrevBlock($('.js-text-card-pushed-top'));
        function addSpaceOnPrevBlock(_node) {
            if (_node.length === 0) return;
            _node.each(function() {
                var $self = $(this);
                $('<div class="gap-p-sm pb-0 hidden-xs-down"></div>').appendTo($self.closest('section').prev());
            });
        }
    })(jQuery);
</script>