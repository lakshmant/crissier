<?php
$title = !empty(get_sub_field('title'))?get_sub_field('title'): '';
$text = !empty(get_sub_field('text'))?get_sub_field('text'):'';
$bgColor = !empty(get_sub_field('background_color')) ? get_sub_field('background_color') : 'white';
?>
<section class="block block--text bg-<?=$bgColor?> is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="row align-items-center">
            <div class="col-sm-4 mb-3 mb-sm-0">
                <div class="card pr-5 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <h2 class="h1 mb-1"><span class="text-primary"><?=$title?></span></h2>
                    <p><?=$text?></p>
                </div>
            </div>
            <div class="col-sm-1 flex-sm-last mb-3 mb-sm-0">
                <div class="card card--picto wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="picto-group justify-content-center justify-content-sm-end">
                        <a href="" class="text-primary"><i class="icon icon-right-arrow icon-md mr-0"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="row">
                    <?php
                     $pictos = get_sub_field('pictos');
                    foreach ($pictos as $picto) :
                    ?>
                    <div class="col-6 col-xs-3 col-sm-3 mb-3 mb-sm-0">
                        <div class="card card--picto wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                            <div class="picto-group flex-column justify-content-center">
                                <i class="icon icon-primary <?=$picto['picto_type']?> icon-xxl pb-1 mr-0 mb-2"></i>
                                <div class="picto-text"> <?=$picto['title']?></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div><!-- /.Block body ends -->
</section><!-- /.Reusable simple text block ends -->
