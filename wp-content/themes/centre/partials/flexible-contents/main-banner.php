<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 10/11/2017
 * Time: 2:34 PM
 */
$photo = get_sub_field('banner_image');
$title = get_sub_field('banner_title');
/*if($photo) $photo = \App\getImageManager()->resize( \App\getImageDirectoryPath($photo), \App\IMAGE_SIZE_CMS_BANNER);*/
if(!empty($photo)) :
    ?>

    <section class="promo promo--bkg text-right gap-p-sm has-cover is-parallaxed is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image:url(<?=$photo?>);">
        <div class="promo__c is-floated center">
            <div class="row justify-content-end align-items-center">
                <div class="col-sm-7">
                    <div class="card text-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                        <?php if(!empty($title)) : ?>
                        <?php endif; ?>
                        <div class="h1 fw-r mb-0"><?=$title?></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if (is_single()) : ?>
    <div class="block block-single-sharer gap-p-xs pb-0 bg-white is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <div class="single-post-sharer d-flex justify-content-end lh-1">
            <?php app_social_share(get_permalink(),get_the_title(),''); ?>
        </div>
    </div>
<?php endif;