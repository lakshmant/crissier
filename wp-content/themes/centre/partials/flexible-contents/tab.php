<?php $bgColor = !empty(get_sub_field('background_color')) ? get_sub_field('background_color') : ''; ?>
<section class="block block--tab gap-p-eq bg-<?=$bgColor?> is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <!-- Tab module -->
        <div class="tab">
            <!-- Tab nav -->
            <nav class="tab-nav hidden-xs-down" role="navigation">
                <!-- Tab nav menu -->
                <ul class="tab-list" role="list">
                    <?php
                    $tabContents = get_sub_field('contents');
                    $tabTitleCount = 1;
                    foreach ($tabContents as $tabTitle ) :
                    ?>
                    <li class="tab-list__i <?php echo ($tabTitleCount==1)?'active':''; ?>">
                        <a href="" class="tab-list__l js-tab-toggler" data-tab-target="tab-pane<?=$tabTitleCount?>"><?=$tabTitle['title']?></a>
                    </li>
                    <?php $tabTitleCount++; endforeach; ?>
                </ul><!-- /.Tab nav menu -->
            </nav><!-- /.Tab nav -->

            <!-- Tab content -->
            <div class="tab__content">
                <div class="accordion js-accordion" data-scroll-active="true">
                    <?php
                    $tabCount = count($tabContents);
                    $count=1;
                     foreach ($tabContents as $content) :
                         if($count==1) :
                            $activeTab = 'active-pane animate ';
                            $accActive = 'active';
                            $h4ExpandedClass = 'expanded';
                         else :
                             $activeTab = '';
                             $accActive = '';
                             $h4ExpandedClass = '';
                        endif;
                    ?>

                    <div class="tab__pane <?=$activeTab?> tab-pane<?=$count?>">
                        <div class="accordion-panel <?=$accActive?>">
                            <div class="accordion-panel__h hidden-sm-up">
                                <h4 class="accordion-panel__t mb-0"><a href="" class="accordion-panel__l js-accordion__toggler <?=$h4ExpandedClass?>" data-toggle-target="js-accordion-target-panel<?=$count?>"><?=!empty($content['title'])?$content['title']:""?></a></h4>
                            </div>
                            <div class="accordion-panel__collapse js-accordion-target-panel<?=$count?>">
                                <div class="accordion-panel__b">
                                    <div class="row">

                                        <?php if(!empty($content['col_1'])) : ?>
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <div class="card">
                                                <?=$content['col_1']?>
                                            </div>
                                        </div>
                                    <?php endif;?>
                                        <?php if(!empty($content['col_2'])) :
                                                 if($tabCount!=$count)  $class = 'mb-3 mb-sm-0';

                                            ?>
                                            <div class="col-sm-6 <?=$class?>">
                                                <div class="card">
                                                    <?=$content['col_2']?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $count++; endforeach; ?>
                </div>
            </div><!-- /.Tab content -->
        </div><!-- /.Tab module ends -->
    </div><!-- /.Blcok body ends -->
</section><!-- /.Reusable tab text block ends -->
