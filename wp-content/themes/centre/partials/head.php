<?php
/*
 * only meta head info
 */
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <?php if($_SERVER['SERVER_NAME']=='centre-migros-crissier.ch') {
        $inlinePropertyID ="5adedf980941940011b04b32";
    }else {
        $inlinePropertyID ="5ab9e3371fff98001395a5b1";
    }
    ?>
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=<?=$inlinePropertyID?>&product=inline-share-buttons"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-P2WX3HJ');</script>
    <!-- End Google Tag Manager -->
   <?php  wp_head(); ?>
</head>