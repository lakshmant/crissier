<footer id="colophon" class="site-footer bg-inverse wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" role="contentinfo">
    <div class="container">
        <div class="site-footer-inner">
            <div class="row m-b-3 m-b-x-3">
                <div class="col-sm-9 flex-sm-last wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="footer-widget footer-widget--services pl-0 pl-sm-4">
                        <div class="text-card text-card--lg bg-white ml-0 ml-sm-1">
                            <header class="text-card__h mb-4 mb-sm-7 text-center">
                                <?php $serviceTitle = get_field('service_title','options'); ?>
                                <div class="h1 mb-0"><?=$serviceTitle?></div>
                            </header>
                            <div class="text-card__b">
                                <ul class="lists lists--2 lists--inline lists--services">
                                    <?php
                                    $args = array(
                                        'post_type' => 'services'
                                    );
                                    $query = new WP_Query( $args );
                                     //selectPic get_field('select_picto');
                                    while ($query->have_posts()):$query->the_post();
                                        $iconPicto = get_field('select_picto');
                                    ?>
                                    <li class="lists__i">
                                        <div class="picto-group"><i class="icon icon-lg icon-primary <?=$iconPicto?>"></i><span class="picto-label"><?php the_title(); ?></span></div>
                                    </li>
                                    <?php endwhile; wp_reset_postdata(); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3 flex-md-first hidden-xs-down wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="footer-widget ff-os text-center text-sm-left">
                        <figure class="mb-4"><a href="<?=home_url()?>?>"><img alt="Site logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_f.png" /></a></figure>
                        <?php $address = get_field('address','options');
                                if(!empty($address)):
                        ?>
                        <address><span class="text-faded"><?=$address?></span></address>
                        <?php endif; ?>
                        <div class="footer-widget ff-os text-center mt-3 mb-3 hidden-xs-down">
                            <ul class="lists d-flex">
                                <li class="mr-2 pr-2 mb-0"><a href="https://www.instagram.com/centremigroscrissier/" class="text-muted" target="_blank"><i class="icon icon-default icon-instagram mr-0"></i></a></li>
                                <li class="mb-0"><a href="https://www.facebook.com/centremigroscrissier/" class="text-muted" target="_blank"><i class="icon icon-default icon-facebook mr-0"></i></a></li>
                            </ul>
                        </div>

						<?php
							$legal_information_content = get_field('legal_information_links', 'options');
							if ( $legal_information_content ) : ?>
								<div class="quick-links-holder">
									<?= $legal_information_content; ?>
								</div>
						<?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="row footer-bottom-row justify-content-end mt-sm-5 text-center-xs">
                <div class="col-sm-8 mb-4 mb-sm-0 hidden-xs-down wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="footer-widget ff-os text-center text-sm-left">
                        <div class="row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <?php $footerTitle1 = get_field('footer_title_1','options');
                                $footerText1 = get_field('footer_text_1','options');
                                ?>
                                <?php if(!empty($footerTitle1)) : ?>
                                    <h4 class="footer-widget__t text-white ls-1"><?=$footerTitle1?></h4>
                                 <?php endif; ?>
                                    <?php if(!empty($footerText1)): ?>
                                        <div class="footer-widget__b text-muted para text-small">
                                            <?=$footerText1?>
                                        </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-6">
                                <?php
                                $footerTitle2 = get_field('footer_title_2','options');
                                $footerText2 = get_field('footer_text_2','options');

                                if(!empty($footerTitle2)) : ?>
                                    <h4 class="footer-widget__t text-white ls-1"><?=$footerTitle2?></h4>
                                <?php endif; ?>
                                <?php if(!empty($footerText2)): ?>
                                <div class="footer-widget__b text-muted para text-small">
                                    <?=$footerText2?>
                                </div>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="footer-widget ff-os text-center text-sm-left" id="newsletter">
                        <h4 class="footer-widget__t text-white ls-1">Newsletter</h4>
                        <div class="para text-small mb-3 text-muted hidden-xs-down">Abonnez-vous a notre newsletter pour etre a laffut de tous les bons plans !</div>
                        <div class="form">
                           <!-- <div id="error-mess"></div>
                            <form method="post" id="form_mailchimp" class="newsletter-form">
                                <div class="form-group mb-0 position-relative pr-6">
                                    <input type="email" name="email" class="form-control newsletter-form__control ff-os" placeholder="Entrez votre adresse e-mail" />
                                    <button role="button" class="newsletter-form__btn"><i class="icon icon-right-arrow text-white"></i></button>
                                </div>
                            </form>-->
                            <?php echo do_shortcode('[sibwp_form id=2]') ?>

                        </div>
                    </div>

                    <div class="footer-widget ff-os text-center mt-4 hidden-sm-up">
                        <a href="" class="js-opening-hours text-uppercase text-white"><strong>heures d’ouvertures</strong><i class="icon icon-right-arrow text-white ml-3 mr-0"></i></a>
                    </div>

                    <div class="footer-widget ff-os text-center mt-4 hidden-sm-up">
                        <ul class="lists d-flex justify-content-center">
                            <li class="mr-3 mb-0"><a href="https://www.instagram.com/centremigroscrissier/" target="_blank"><i class="icon icon-lg icon-faded icon-instagram mr-0"></i></a></li>
                            <li class="mb-0"><a href="https://www.facebook.com/centremigroscrissier/" target="_blank"><i class="icon icon-lg icon-faded icon-facebook mr-0"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php if($_SERVER['SERVER_NAME']=='centre-migros-crissier.ch') {
    $fbAppID ="238867293518129";
}else {
    $fbAppID ="410871346021549";
}
?>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '<?php echo $fbAppID ?>',
            xfbml      : true,
            version    : 'v2.12'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    function fbShare(url){
        console.log("fb share");
        FB.ui({
            method: 'share',
            href: url,
        }, function(response){});
    }

</script>

<script>
    <?php
    $holidays = get_field("holidays","options");
    $holidayMonths = [];
    foreach ($holidays as $holiday){
        $monthId = (int) $holiday['public_holidays_month'];
        //if(!array_key_exists($monthId, $holidayMonths)){
        //explode the dates by comma and convert all the values inside the array to integer
        $holidayMonths[$monthId] = array_map('intval', explode(',', $holiday['pubic_holiday_days']));
        //}
    }
    $holidayMonths = json_encode($holidayMonths);
    ?>
    var publicHolidays = <?=$holidayMonths?>;
  var ShopTime = {
        1 : [8, 20],     //mon
        2 : [8, 20],    //tue
        3 : [8, 20],  //wed
        4 : [8, 20],   //thur
        5 : [8, 21],     //fri
        6 : [7.5, 18],        //sat
    };

    var today = new Date();
    var ShopTiming = new function(){
        this.getStatus = function(){
            var now = new Date();
            var currentHour = now.getHours() + now.getMinutes()/60;
            var currentDay = now.getDay();
            if(currentDay == 0){
                return null;
            }
            var currentShopTime = ShopTime[currentDay];
            console.log(currentDay, currentHour, currentShopTime)
            if(currentHour > currentShopTime[0] && currentHour < currentShopTime[1]){
                return currentShopTime;
            }else{
                return null;
            }
        }
    };

    var todaysShopTime = ShopTiming.getStatus();
    if(todaysShopTime != null){
        //check on public holidays...
        if((typeof(publicHolidays[today.getMonth() + 1]) != 'undefined')){
            //check current day is listed as holiday on this particular month
            if(publicHolidays[today.getMonth() + 1].indexOf(today.getDate()) > -1){
                todaysShopTime = null;
            }
        }
    }

    if(todaysShopTime == null){
        document.getElementById('openingHoursDropdown').classList.add('shop-closed');
        document.querySelector('.js-label-shop-status').innerHTML = 'FERME';
        document.getElementById("js-closed-hours").style.display = "none";
    }else{
       // document.getElementById('openingHoursDropdown').style.color = '#e1568e';
        document.getElementById('openingHoursDropdown').classList.add('text-primary');
        /**
         * @reminder :: IE doesn't support forEach constructor, so please add it on prototype of the array
         */
        Array.prototype.forEach.call(document.querySelectorAll('.js-shop-close-time'), function(ele){
            ele.innerHTML = todaysShopTime[1]+'H';
        });
    }
</script>

<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">
    stLight.options({publisher:'5981615a5bfb5500122a58e5'});</script>