<section class="block block--hero position-relative hidden-xs-down wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="swiper-container slider slider--hero slider--hero-style1 js-slider-hero">
            <div class="swiper-wrapper slider__wrapper">
                <?php
                $sliderImages = get_field('slider',$post->ID);
                foreach ($sliderImages as $images) {
                    $image =  !empty($images['image'])?$images['image']:'';
                    if($image)  :
                        ?>
                        <div class="swiper-slide slider__slide">
                            <div class="slider__b">
                                <figure class="slider__pic mb-0 text-center"><img alt="Hero Slider Image" src="<?php echo $image; ?>" /></figure>
                            </div>
                        </div>
                    <?php endif; } ?>
            </div>
            <div class="swiper-button swiper-button-next shape-sq"><i class="icon icon-right-chev mr-0"></i></div>
            <div class="swiper-button swiper-button-prev shape-sq"><i class="icon icon-left-chev mr-0"></i></div>
        </div>
        <div class="hero-overlay-content wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
            <div class="shape opening-hours-card js-opening-hours text-white" id="js-closed-hours">
                <div class="shape__c">
                    <div class="h5">Ouvert <br> aujourd’hui jusqu’a</div>
                    <div class="h1 mb-0 ff-pp fw-b"><strong class="js-shop-close-time"></strong></div>
                    <i class="icon icon-plus icon-xs icon-light fw-b mr-0"></i>
                </div>
            </div>
            <div class="shape marker-holder">
                <div class="shape__c">
                    <i class="icon icon-pin-outline icon-light fw-b mr-0"></i>
                </div>
                <a href="<?=get_field('google_map_link',$post->ID)?>" class="link-stacked" target="_blank"></a>
            </div>
        </div>
    </div>
</section>

<?php

$smartImage = get_field('smart_image',$post->ID);
if(!empty($smartImage)):
    $subText = get_field('smart_text',$post->ID);
    ?>
<div class="xs-promo position-relative text-white text-center hidden-sm-up">
    <div class="xs-promo__body">
        <figure class="xs-promo__pic mask-overlay mb-0">
            <img alt="Mobile Banner Alt Text" class="img xs-promo__img" src="<?php echo $smartImage; ?>" />
        </figure>

        <div class="xs-promo__c is-floated center">
            <?php if(!empty($subText)): ?>
            <div class="text-holder mb-6">
                <div class="xs-promo__subtitle"><?=$subText?></div>
                <div class="xs-promo__title"><strong  class="js-shop-close-time"></strong></div>
            </div>
            <?php endif;
            $sCtaLink = get_field('smart_cta_link',$post->ID);
            $sCtaTitle = get_field('smart_cta_label',$post->ID);
            if(!empty($sCtaLink)):
            ?>
            <div class="btn-holder">
                <a href="<?=$sCtaLink?>" class="btn btn-outline-faded border-fat xs-promo__btn"><?=$sCtaTitle?></a>
            </div>
            <?php endif; ?>
        </div>
        <div class="xs-promo__f is-floated bottom">
            <a href="https://maps.google.com/?q=Chemin+de+Closalet+7+1023+Crissier&46.554810, 6.569259&ll=46.554810, 6.569259&z=18" class="btn btn-primary map-btn" target="_blank"><i class="icon icon-pin-outline icon-light fw-b"></i>Trouvez-Nous</a>
        </div>
    </div>
</div>
<?php endif;