<?php
    $backgroundImage = get_field('background_image',$post->ID);
    $bannerImage = get_field('banner_image',$post->ID);
    $bannerTitle = get_field('banner_title',$post->ID);
    $bannerText = get_field('banner_text',$post->ID);
    $bannerLink = get_field('banner_link',$post->ID);
?>
<section class="promo promo--bkg promo--bkg-lg gap-p-lg has-cover is-parallaxed is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image:url(<?=$backgroundImage?>);">
    <div class="promo__c is-floated center">
        <div class="d-flex flex-wrap no-gutters align-items-center">
            <div class="col-sm-7 mb-4 mb-sm-0 text-center text-sm-left">
                <div class="card shadowed wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                    <?php if(!empty($bannerLink)&& !empty($bannerImage)) : ?>
                            <figure class="mb-0">
                                <a href="<?=$bannerLink?>"><img alt="Promotion image" class="img img-xs-full img-fluid" src="<?=$bannerImage?>" /></a>
                            </figure>
                        <?php elseif (!empty($bannerImage) && empty($bannerLink)) : ?>
                            <figure class="mb-0">
                                <img alt="Promotion image" class="img img-xs-full img-fluid" src="<?=$bannerImage?>" />
                            </figure>
                        <?php endif; ?>
                </div>
            </div>
            <div class="col-sm-5 col-md-4 pl-sm-5 pl-lg-4 text-center text-sm-left">
                <div class="card has-ribbon d-block d-sm-inline-block text-white text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                    <div class="position-relative mt-0 mt-sm-2">
                        <?php if(!empty($bannerTitle)) : ?>
                            <h2><?=$bannerTitle?></h2>
                        <?php endif; ?>
                        <p class="ff-os"><?=$bannerText?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>