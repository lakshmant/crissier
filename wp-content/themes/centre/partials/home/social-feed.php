﻿<?php
$instaTitle1 = get_field('instagram_title_1');
$instaTitle2 = get_field('instagram_title_2');
function getInstagramFeeds($limit = 1){

    $api_url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=6931956446.216fb75.381885acb2ac4ccb84b9b39dd4b363ef&count=".$limit;
    $connection_c = curl_init(); // initializing
    curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
    curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
    curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
    $json_return = curl_exec( $connection_c ); // connect and get json data
    curl_close( $connection_c ); // close connection
    $insta = json_decode( $json_return ); // decode and return

    foreach($insta->data as $feed){
        //thumbnail
        // low_resolution
        // standard_resolution
        $items[] = array($feed->link, $feed->images->standard_resolution->url);
    }
    //var_dump($items);
    return $items;
}



$instaItems = getInstagramFeeds(5);


/**
*   @todo: refactor the code
*   @desc: Builds instagram grid on page according to the condition being applied
*   @param: { array } - $instaItems - Array of instagram feeds
*   @param: { string } - $slider_slide_class - Swiper slider slide class
*   @param: { boolean } - $flag_hide_title - Flag to hide unnecessary blocks in slider which causes issue
*   @param: { boolean } - $has_animation - Flag to toggle wow animation class
*/
function instagramBlocks($instaItems, $flag_hide_title ){
    $c=1;
    if ( $flag_hide_title === true ) : 
        $slider_slide_class = 'swiper-slide';
    else :
        $has_animation = true;
    endif;
    foreach($instaItems as $instaImage):
        $instaLink = $instaImage[0];
        $instaImg = $instaImage[1];
        if($c==1) :
            ?>
            <div class="col social-media-col <?= isset($slider_slide_class) ? $slider_slide_class : ''; ?> sm">
                <div class="card card--sq <?= $has_animation === true ? "wow fadeInDown" : 'wow-disabled'; ?>" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c px-0 py-0">
                        <figure class="zoom-effect-holder mb-0"><img alt="Instagram Image" class="img zoom-effect img-fluid" src="<?=$instaImg?>" /></figure>
                    </div>
                    <a href="<?=$instaLink?>" target="_blank" class="link-stacked"></a>
                </div>
            </div>
            <?php if ( $flag_hide_title === false ) : ?>
            <div class="col social-media-col sm hidden-xs-down">
                <div class="card card--sq bg-faded wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c px-2 py-2 center">
                        <div class="picto-group justify-content-center mb-2"><i class="icon icon-instagram icon-faded icon-lg mb-2 mr-0"></i></div>
                        <div class="h5 text-muted mb-0 ls-1">Instagram</div>
                    </div>
                    <a href="https://www.instagram.com/centremigroscrissier/" target="_blank" class="link-stacked"></a>
                </div>
            </div>
            <?php endif;
        elseif ($c==2):?>
            <div class="col social-media-col <?= isset($slider_slide_class) ? $slider_slide_class : ''; ?> lg">
                <div class="card card--sq is-floated <?= isset($has_animation) && $has_animation === true ? "wow fadeInDown" : 'wow-disabled'; ?>" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c px-0 py-0">
                        <figure class="zoom-effect-holder mb-0"><img alt="Instagram Image" class="img zoom-effect img-fluid" src="<?=$instaImg?>"/></figure>
                        <a href="<?=$instaLink?>" target="_blank" class="link-stacked"></a>
                    </div>
                </div>
            </div>
        <?php elseif ($c==3):?>
            <div class="col social-media-col <?= isset($slider_slide_class) ? $slider_slide_class : ''; ?> sm push-right">
                <div class="card card--sq <?= isset($has_animation) && $has_animation === true ? "wow fadeInDown" : 'wow-disabled'; ?>" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c px-0 py-0">
                        <figure class="zoom-effect-holder mb-0"><img alt="Instagram Image" class="img zoom-effect img-fluid" src="<?=$instaImg?>" /></figure>
                        <a href="<?=$instaLink?>" target="_blank" class="link-stacked"></a>
                    </div>
                </div>
            </div>
        <?php elseif ($c==4): ?>
            <div class="col social-media-col <?= isset($slider_slide_class) ? $slider_slide_class : ''; ?> sm push-left">
                <div class="card card--sq <?= isset($has_animation) && $has_animation === true ? "wow fadeInDown" : 'wow-disabled'; ?>" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c px-0 py-0">
                        <figure class="zoom-effect-holder mb-0"><img alt="Instagram Image" class="img zoom-effect img-fluid"  src="<?=$instaImg?>" /></figure>
                        <a href="<?=$instaLink?>" target="_blank" class="link-stacked"></a>
                    </div>
                </div>
            </div>
        <?php elseif ($c==5):
            if ( $flag_hide_title === false) : ?>
            <div class="col social-media-col sm push-left hidden-xs-down">
                <div class="card card--sq bg-faded wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c px-2 py-2 center">
                        <div class="picto-group justify-content-center mb-2"><i class="icon icon-facebook icon-faded icon-lg mb-2 mr-0"></i></div>
                        <div class="h5 text-muted mb-0 ls-1">Facebook</div>
                    </div>
                    <a href="https://www.facebook.com/CentreMigrosCrissier/" target="_blank" class="link-stacked"></a>
                </div>
            </div>
            <?php endif; ?>
            <div class="col social-media-col <?= isset($slider_slide_class) ? $slider_slide_class : ''; ?> sm push-left">
                <div class="card card--sq <?= isset($has_animation) && $has_animation === true ? "wow fadeInDown" : 'wow-disabled'; ?>" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                    <div class="card__c px-0 py-0">
                        <figure class="zoom-effect-holder mb-0"><img alt="Instagram Image" class="img zoom-effect img-fluid"  src="<?=$instaImg?>" /></figure>
                        <a href="<?=$instaLink?>" target="_blank" class="link-stacked"></a>
                    </div>
                </div>
            </div>
        <?php  endif;
        $c++;
    endforeach;
}
?>
<section id="instagramBlock" class="block block--instagram last wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
    <div class="block__b">
        <div class="card-holder social-media-container">
            <div class="card xs-insta-card bg-white shadowed hidden-sm-up wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                <div class="card__c position-relative center">
                    <div class="picto-group is-floated left"><i class="icon icon-instagram icon-faded icon-xl mr-0"></i></div>
                    <?php if(!empty($instaTitle1)) ?><div class="h1 mb-0"><?=$instaTitle1?></div>
                    <?php if(!empty($instaTitle2)) ?><div class="ff-os text-sm-small"><?=$instaTitle2?></div>
                </div>
            </div>
            <div class="swiper-container swiper-insta slider-sm js-slider-sm hidden-sm-up">
                <div class="swiper-wrapper">
                    <?php echo instagramBlocks($instaItems, true ); ?>
                </div>
                <div class="swiper-pagination swiper-pagination--custom hidden-sm-up"></div>
            </div>
            <div class="no-gutters d-flex flex-wrap social-media-grid hidden-xs-down">
                <div class="col social-media-col sm hidden-xs-down"></div>
                <div class="col social-media-col sm title mt-offset-4">
                    <div class="card card--sq overflow-visible zindex-2 bg-white shadowed has-arrow down left-bottom hidden-xs-down wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="card__c px-2 py-2 center">
                            <?php if(!empty($instaTitle1)) ?><div class="h1 mb-0"><?=$instaTitle1?></div>
                            <?php if(!empty($instaTitle2)) ?><div class="ff-os text-sm-small"><?=$instaTitle2?></div>
                        </div>
                    </div>
                </div>
                <div class="col social-media-col lg hidden-sm-down"></div>
                <?php echo instagramBlocks($instaItems, false ); ?>
                <div class="col social-media-col sm last push-right hidden-xs-down">
                    <div class="card card--sq wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="shape-sq picto-holder bg-primary d-inline-block"><i class="icon icon-light icon-right-chev card-icon mr-0"></i><a href="https://www.instagram.com/centremigroscrissier/" class="link-stacked"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>