<section id="homeNewsBlock" class="block block--mag pb-4 pb-sm-0 has-cover is-extended position-relative zindex-2 is-parallaxed wow fadeIn" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/mag-bkg.jpg);">
    <header class="block__h wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <div class="row no-gutters justify-content-end">
            <div class="col-sm-8">
            <div class="h1 mb-0 text-white text-center hidden-sm-up"> <?php echo get_field('mag_title_2',$post->ID); ?></div>
                <div class="text-card text-card--xl has-arrow down left-bottom white bg-white mt-offset-eq hidden-xs-down">
                    <div class="h5 text-muted ls-1 mb-3"> <?php echo get_field('mag_title_1',$post->ID); ?></div>
                    <div class="h1 mb-0"> <?php echo get_field('mag_title_2',$post->ID); ?></div>
                    <p class="ff-os"><?php echo get_field('mag_text',$post->ID); ?></p>
                </div>
            </div>
        </div>
    </header>
    <div class="block__b pt-3 pt-sm-12">
        <div class="card-holder position-relative">
            <div class="text-offset text-center mb-3 ls-1 hidden-xs-down wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20"><a href="<?php echo home_url('/le-mag/'); ?>" class="text-offset-link"><i class="icon icon-plus"></i>Voir tous les articles</a></div>
            <div class="row">
                <?php
                $count=1;
                $query = new WP_Query( array( 'post_type' => 'post','posts_per_page' =>4, 'post_status'=>'publish' ) );
                while($query->have_posts()): $query->the_post();
                    $relatedCat = get_the_terms($query->post->ID,'category');
                    $imgSrc = wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'full', false );
                    $largeImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($imgSrc[0]), \App\IMAGE_SIZE_NEWS_LARGE);
                    $smallImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($imgSrc[0]), \App\IMAGE_SIZE_NEWS_SMALL);
                    $categories = get_the_terms($post->ID,'category');
                    $catName = $categories[0]->name;
                ?>
                <?php if($count==1) : ?>
                        <div class="col-12 card-col card-col--7 mb-pull-4">
                    <?php
                    elseif($count==3) :?>
                        <div class="col-12 card-col card-col--5 mt-pull-8 hidden-xs-down">
                <?php  endif;?>
                <?php if($count == 1 ||  $count == 2 || $count == 4) :
                        if($count == 2 || $count == 4) :
                            $articleTwoClass ='mb-sm-0 ';
                        endif;
                    ?>
                    <article class="article-card bg-white <?php echo isset($articleTwoClass) ? $articleTwoClass : 'mb-3'; ?> wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <div class="article-card-inner">
                            <?php if(!empty($largeImage)) : ?>
                            <figure class="article-card__pic zoom-effect-holder mb-0">  <a href="<?php the_permalink(); ?>"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo $largeImage; ?>" /></a> </figure>
                            <?php endif; ?>
                            <div class="article-card__c">
                                <?php if(!empty($catName)) : ?><div class="h5 promo-cat"><a href="<?php the_permalink(); ?>" class=" text-muted"><?=$relatedCat[0]->name?></a></div>
                                <?php endif;?>
                                <h2 class="promo-title"><a href="<?php the_permalink(); ?>" class=" text-dark"><?php the_title(); ?></a></h2>
                                <div class="promo-tags ff-os"><?php App\kd_related_terms($query->post->ID); ?></div>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                <i class="icon icon-light icon-plus card-icon mr-0"></i>
                            </a>
                        </div>
                    </article>
                    <?php else :
                        if($count==3) :
                            $articleClass ="mb-3 ";
                            endif;
                        ?>
                        <article class="article-card bg-white <?=$articleClass?>wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                            <div class="article-card-inner">
                                <?php if(!empty($smallImage)): ?>
                                <figure class="article-card__pic zoom-effect-holder mb-0">
                                    <a href="<?php the_permalink(); ?>"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?php echo $smallImage; ?>" /></a>
                                </figure>
                                <?php endif; ?>
                                <div class="article-card__c">
                                    <?php if(!empty($catName)) : ?><div class="h5 promo-cat"><a href="<?php the_permalink(); ?>" class=" text-muted"><?=$relatedCat[0]->name?></a></div>
                                    <?php endif; ?>
                                    <h2 class="promo-title"><a href="<?php the_permalink(); ?>" class=" text-dark"><?php the_title(); ?></a></h2>
                                    <div class="promo-tags ff-os"><?php App\kd_related_terms($query->post->ID); ?></div>
                                </div>
                                <a href="<?php the_permalink(); ?>" class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                                    <?php if($count==3) :
                                        ?>
                                        <i class="icon icon-light icon-plus card-icon mr-0"></i>
                                    <?php else : ?>
                                                <div class="shape-sq picto-holder  bg-primary d-inline-block">
                                                    <i class="icon icon-light icon-plus card-icon mr-0"></i>
                                                </div>
                                      <?php endif;  ?>
                                </a>
                        </article>
                   <?php endif; ?>
                    <?php
                    if($count==2 || $count==4) : ?>
                            </div>
                <?php endif;
                 $count++;
                endwhile; wp_reset_postdata(); ?>
            </div>
            <div class="text-center mt-4 ls-1 hidden-sm-up wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20"><a href="<?php echo home_url('/le-mag/'); ?>" class="text-offset-link text-uppercase"><i class="icon icon-plus"></i>Voir tous les articles</a></div>
        </div>
    </div>
</section>