<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 1/18/2018
 * Time: 5:31 PM
 */
$promoID =  get_field('select_home_promotion',$post->ID);
$promoImage = get_field('image',$promoID);
$title1 = get_the_title($promoID);
$title2 = get_field('promotion_title_2',$promoID);
$price = get_field('price',$promoID);
$text = get_field('text',$promoID);
$logoTermID = get_the_terms($promoID,'logo');
if(!empty($logoTermID[0]->term_id)) :
    $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
    $promoLogoImage = $promoLogoImage['url'];
endif;
$promotionPageID = get_field('select_promotion_page','options');
$promotionPageLink = get_permalink($promotionPageID);
$promoCat = get_the_terms($promoID,'promo_cat');
$promoCatName = $promoCat[0]->name;
$permalink = get_the_permalink($promoID);

if(!empty($promoID)):
    ?>
    <section class="block block--promotion pt-sm-0 gap-p-lg bg-faded position-relative is-extended">
        <div class="block__b">
            <!-- Promotion card holder -->
            <div class="card-holder">
                <div class="row no-gutters card-row">
                    <div class="col-12 card-col card-col--7">
                        <?php if(!empty($promoImage)) : ?>
                            <div class="promo-card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                                <?php if(!empty($promoImage)): ?>
                                    <figure class="promo-card__pic mb-0 zoom-effect-holderx"><img alt="Promotion Image" class="img img-fuild zoom-effectx" src="<?=$promoImage?>" /></figure>
                                <?php endif; ?>
                                <a href="<?=$permalink?>" class="link-stacked"  data-toggle="modal" data-target="#promoBlock1Modal"></a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-12 card-col card-col--5">
                        <div class="promo-card promo-card--text promo-card--text-sm bg-white mt-offset-5 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                            <header class="promo-card__h mb-2 text-right">
                                <?php app_social_share($permalink,$title1,''); ?>
                            </header>
                            <div class="promo-card__c text-center">
                                <div class="row no-gutters justify-content-center">
                                    <div class="col-sm-10 col-md-8">
                                        <?php if(!empty($promoLogoImage)): ?>
                                            <figure class="promo-brand"><img alt="" class="img img-fluid" src="<?=$promoLogoImage?>" /></figure>
                                        <?php endif; ?>
                                        <?php if(!empty($title2)) : ?>
                                            <div class="h5 promo-cat text-muted"><?=$title2?></div>
                                        <?php endif; ?>
                                        <?php if(!empty($title1)) : ?>
                                            <h2 class="promo-title mb-5"><?=$title1?></span></h2>
                                        <?php endif; ?>
                                        <?php if(!empty($price)) : ?>
                                            <div class="promo-price h5 mb-0 ls-1"><?=$price?>-</div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <footer class="promo-card__f is-floated right-bottom text-right">
                                <div class="shape-sq picto-holder bg-primary d-inline-block">
                                    <i class="icon icon-light icon-plus card-icon mr-0"></i>
                                </div>
                            </footer>
                            <a href="javascript:void(0);" class="link-stacked"  data-toggle="modal" data-target="#promoBlock1Modal"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
      //  $homeLayerText1 = get_field("cta_text",$promoID );
       // $homeLayerText2 = get_field("date_offer_condition",$promoID);
        $bannerImage = get_field('image', $promoID);
        $popUpContentPromotion = get_field('popup_contents',$promoID);
        $pdfGeneratorLink = home_url().'/services/promotion.php?id='.$promoID;
        $title =$popUpContentPromotion['title'];
        $text = $popUpContentPromotion['text'];
        $popupLogo = $promoLogoImage;
        $popupDescription = $popUpContentPromotion['description'];
        $link = $popUpContentPromotion['link'];
       // $permalink = get_permalink($promotionPageID);
      //  \App\promotion_layer_content('promoBlock1Modal',$shopPermalink,$promoLogoImage ,$homeLayerText1 ,$homeLayerText2);
        \App\app_promotion_layer_content('promoBlock1Modal' ,$bannerImage ,$pdfGeneratorLink ,$title ,$text ,$popupLogo ,$popupDescription, $promotionPageLink, $link );
        ?>
    </section>
    <?php

endif;

$block2PostID = get_field('select_posts_block_2');
if(!empty($block2PostID)):
    $query = new WP_Query( array( 'p' => $block2PostID ) );
    while ($query->have_posts()):$query->the_post();
        $backgroundImage = get_field('background_image',$block2PostID);
        $permalink = get_the_permalink($block2PostID);
        $title = get_the_title();
        ?>
        <section id="homePromotionBanner" class="promo promo--bkg bg-faded position-relative has-cover is-parallaxed is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image: url('<?=$backgroundImage?>'); visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: fadeInDown;">
            <div class="block__b">
                <div class="card-holder">
                    <div class="row no-gutters card-row">
                        <div class="col-12 card-col card-col--7">
                            <div class="promo-card promo-card--text promo-card--text-lg bg-white mt-offset-3 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.25s; animation-name: fadeInDown;">
                                <div class="promo-card-inner d-flex d-sm-block">
                                    <header class="promo-card__h mb-xs-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="promo-date text-muted d-inline-block"><span class="prmot-date-d"><?php the_date('d'); ?></span><?php /*<span class="prmot-date-m"><?php echo get_the_date('y'); */ ?></span><span class="h2 prmot-date-m mb-0"><?php echo get_the_date('M'); ?>.</span></div>
                                            </div>
                                            <div class="col-6 position-xs-static text-right">
                                                <?php app_social_share($permalink,$title,''); ?>
                                            </div>
                                        </div>
                                    </header>
                                    <div class="promo-card__c px-2 px-sm-0 mx-3 mx-sm-0">
                                        <div class="row no-gutters">
                                            <div class="col-sm-9 col-md-7 col-lg-8">
                                                <h2 class="promo-title"><?php the_title(); ?></h2>
                                                <div class="promo-desc mb-5 ff-os"><?php echo get_the_content(); ?></div>
                                                <div class="h5 promo-cat mb-0">
                                                    <?php $cat = get_the_category($post->ID);
                                                    echo $cat[0]->cat_name;?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="promo-card__f is-floated right-bottom text-right">
                                        <div class="shape-sq picto-holder bg-primary d-inline-block">
                                            <i class="icon icon-light icon-plus card-icon mr-0"></i>
                                        </div>
                                    </footer>
                                </div>

                                <?php if(!empty($backgroundImage)): ?>
                                    <div class="promo-card-xs-holder hidden-sm-up">
                                        <figure class="promo-card-xs__pic mb-0">
                                            <img alt="Alt" class="img img-full promo-card-xs__img" src="<?php echo $backgroundImage; ?>" />
                                        </figure>
                                    </div>
                                <?php endif; ?>

                                <a href="<?php the_permalink(); ?>" class="link-stacked"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    <?php endwhile; wp_reset_postdata(); ?>
<?php endif; ?>

<?php
$promotionLeft = get_field('select_promotion_left',$post->ID);
$promotionRight = get_field('select_promotion_right',$post->ID);

if(!empty($promotionLeft) || !empty($promotionRight)) :
    ?>

    <section class="block block--promotion gap-p-lg bg-faded is-extended">
        <div class="block__b">
            <div class="card-holder">
                <div class="row">
                    <div class="col-12 d-sm-flex  card-col card-col--7 mb-3 mb-sm-0">
                        <?php
                        $promoImage = get_field('image',$promotionLeft);
                        $title1 = get_the_title($promotionLeft);
                        $title2 = get_field('promotion_title_2',$promotionLeft);
                        $price = get_field('price',$promotionLeft);
                        $text = get_field('text',$promotionLeft);
                        $logoTermID = get_the_terms($promotionLeft,'logo');
                        if(!empty($logoTermID[0]->term_id)) :
                            $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
                            $promoLogoImage = $promoLogoImage['url'];
                        endif;
                        $promoCat = get_the_terms($promotionLeft,'promo_cat');
                        $promoCatName = $promoCat[0]->name;
                        ?>
                        <div class="promo-card promo-card--text promo-card--text-lg bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                            <div class="promo-card__c px-0 py-0">
                                <div class="row align-items-center text-center">
                                    <div class="col-sm-6 mb-5 mb-sm-0">
                                        <?php if(!empty($promoLogoImage)) : ?>
                                            <figure class="promo-brand mt-0"><img alt="" class="img img-fluid" src="<?php echo $promoLogoImage; ?>" /></figure>
                                        <?php endif; ?>
                                        <?php if (!empty($promoCatName)) : ?>
                                            <div class="h5 promo-cat text-muted"><?=$promoCatName?></div>
                                        <?php endif; ?>
                                        <h2 class="promo-title mb-0"><?=$title1?></h2>

                                        <?php if (!empty($price)) : ?>
                                            <div class="promo-price h5 mt-5 mb-0 ls-1"><?=$price?>-</div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <figure class="promo-card__pic mb-0"><img alt="" class="img img-fluid" src="<?php echo $promoImage; ?>" /></figure>
                                    </div>
                                </div>
                            </div>
                            <footer class="promo-card__f is-floated right-bottom text-right">
                                <div class="shape-sq promo-picto bg-primary d-inline-block">
                                    <i class="icon icon-light icon-plus card-icon mr-0"></i>

                                </div>
                            </footer>
                            <a href="javascript:void(0);" class="link-stacked"  data-toggle="modal" data-target="#promoModal2"></a>
                            <?php
                        /*    $leftLayerText1 = get_field("cta_text",$promotionLeft );
                            $leftLayerText2 = get_field("date_offer_condition",$promotionLeft);
                            $shopPermalink =   get_related_term_post_permalink($logoTermID[0]->term_id);
                            */

                            $bannerImage = get_field('image', $promotionLeft);
                            $popUpContentPromotion = get_field('popup_contents',$promotionLeft);
                            $pdfGeneratorLink = home_url().'/services/promotion.php?id='.$promotionLeft;
                            $title =$popUpContentPromotion['title'];
                            $text = $popUpContentPromotion['text'];
                            $popupDescription = $popUpContentPromotion['description'];
                            $link = $popUpContentPromotion['link'];
                            //$permalink = get_permalink($promotionPageID);

                            \App\app_promotion_layer_content('promoModal2' ,$bannerImage ,$pdfGeneratorLink ,$title ,$text ,$promoLogoImage ,$popupDescription ,$promotionPageLink, $link );

                          //  \App\promotion_layer_content('promoModal2',$shopPermalink,$promoLogoImage ,$leftLayerText1 ,$leftLayerText2);
                            ?>
                        </div>
                    </div>

                    <div class="col-12 d-sm-flex card-col card-col--5">
                        <?php
                        $promoImage = get_field('image',$promotionRight);
                        $price = get_field('price',$promotionRight);
                        $text = get_field('text',$promotionRight);
                        $logoTermID = get_the_terms($promotionRight,'logo');
                        if(!empty($logoTermID[0]->term_id)) :
                            $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
                            $promoLogoImage = $promoLogoImage['url'];
                        endif;

                        $promoCat = get_the_terms($promotionRight,'promo_cat');
                        $promoCatName = $promoCat[0]->name;
                        //  CONVERT THE FIELD FROM ACF TO UNIX TIMESTAMP
                        $unixtimestamp = strtotime(get_field('promotion_date',$promotionRight));
                        ?>
                        <div class="promo-card promo-card--text promo-card--text-sm bg-white wow  fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                            <header class="promo-card__h mb-3">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="promo-date text-muted d-inline-block"><span class="prmot-date-d"><?php echo date_i18n('d', $unixtimestamp); ?></span><span class="h2 prmot-date-m mb-0"><?php echo date_i18n('M', $unixtimestamp);?>.</span></div>
                                    </div>
                                    <div class="col-6 text-right">
                                        <?php app_social_share(get_the_permalink($promotionRight),get_the_title($promotionRight),''); ?>
                                    </div>
                                </div>
                            </header>
                            <div class="promo-card__c px-0 py-0">
                                <h2 class="promo-title mb-0"><?php echo get_the_title($promotionRight); ?> </h2>
                                <?php if(!empty($promoCatName)) : ?>
                                    <div class="h5 promo-cat mt-5 mb-0"><?=$promoCatName?></div>
                                <?php endif; ?>
                            </div>
                            <footer class="promo-card__f is-floated right-bottom text-right">
                                <div class="shape-sq promo-picto bg-primary d-inline-block">
                                    <i class="icon icon-light icon-plus card-icon mr-0"></i>

                                </div>
                            </footer>
                            <a href="javascript:void(0);" class="link-stacked"  data-toggle="modal" data-target="#promoModal3"></a>
                            <?php
                            $rightLayerText1 = get_field("cta_text",$promotionRight);
                            $rightLayerText2 = get_field("date_offer_condition",$promotionRight);
                            $logoTermID = get_the_terms($promotionRight,'logo');

                            if(!empty($logoTermID[0]->term_id)) :
                                $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
                                $promoLogoImage = $promoLogoImage['url'];
                            endif;

                            $bannerImage = get_field('image', $promotionRight);
                            $popUpContentPromotion = get_field('popup_contents',$promotionRight);
                            $pdfGeneratorLink = home_url().'/services/promotion.php?id='.$promotionRight;
                            $title =$popUpContentPromotion['title'];
                            $text = $popUpContentPromotion['text'];
                            $popupDescription = $popUpContentPromotion['description'];
                            $link = $popUpContentPromotion['link'];

                            \App\app_promotion_layer_content('promoModal3' ,$bannerImage ,$pdfGeneratorLink ,$title ,$text ,$promoLogoImage ,$popupDescription ,$promotionPageLink, $link );
                         /*   $shopPermalink =   get_related_term_post_permalink($logoTermID[0]->term_id);
                            \App\promotion_layer_content('promoModal3', $shopPermalink,$promoLogoImage, $rightLayerText1,$rightLayerText2);*/
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;