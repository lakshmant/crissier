<section id="newsletterBlock" class="block gap-p-lg pt-sm-0 bg-faded position-relative is-extended">
    <div class="block__b">
        <div class="card-holder">
            <div class="row no-gutters card-row">
                <div class="col-12 card-col card-col--7">
                    <div class="promo-card wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
                        <?php
                        $contestTitle = get_field('contest_title_1',$post->ID);
                        $contestLink = get_field('contest_cta',$post->ID);
                        $contestLinkTitle = get_field('cta_title',$post->ID);

                        ?>
                        <div class="is-floated right">
                            <?php  app_social_share($contestLink,$contestTitle,''); ?>
                        </div>
                        <figure class="promo-card__pic mb-0 zoom-effect-holder"><img alt="Promotion Image" class="img img-full img-fuild zoom-effect" src="<?=get_field('contest_image',$post->ID)?>" /></figure>
                        <div class="promo-card__c is-floated center w-100">
                            <div class="d-flex no-gutters justify-content-center">
                                <div class="col-sm-8 col-md-9">
                                    <div class="text-holder text-center text-white">
                                        <div class="fs-45 ff-knev"><?=$contestTitle?></div>
                                        <div class="h2"><?=get_field('contest_title_2',$post->ID)?></div>
                                            <?php if(!empty($contestLink)): ?> <div class="mt-5 mt-sm-12"><a href="<?=$contestLink?>" class="btn btn-mw btn-outline-faded border-fat"><strong><?=$contestLinkTitle?></strong></a></div><?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 card-col card-col--5">
                    <div class="promo-card promo-card--lg bg-primary has-texture mt-offset-4 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                        <div class="form-holder text-white text-center" id="newsletter-contest">
                            <i class="icon icon-newsletter icon-xl icon-light mb-3 mr-0"></i>
                            <div class="h2 mb-2">restez informé</div>
                            <div class="ff-os fw-l">Soyez à l’affût de tous les bons plans, abonnez-vous !</div>
                            <div class="form mt-5">

                                <!--<form method="post" id="form_mailchimp_contest" class="subscribe-form">
                                    <div class="form-group position-relative">
                                        <input class="form-control form-control--lg border-fat border-white" type="email" name="email" placeholder="Entrez votre email" />
                                        <button type="submit" class="form__btn is-floated"><i class="icon icon-right-chev icon-light icon-xs mr-0"></i></button>
                                    </div>
                                </form>-->
                                <?php echo do_shortcode('[sibwp_form id=3]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>