<?php
   $bannerImg = get_field('banner_image_8', $post->ID);
  $bannerText = get_field('banner_text_8',$post->ID);
  $bannerLink = get_field('banner_link_8',$post->ID);
  $bannerTitle = get_field('banner_title_8',$post->ID);
?>
<section id="homePromoMisc" class="promo promo--bkg promo--bkg-lg position-relative gap-p-lg has-cover is-parallaxed is-extended wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="background-image:url(<?=$bannerImg?>);">
    <div class="promo__body">
        <figure class="xs-promo__pic hidden-sm-up mb-0">
            <img alt="Mobile Banner Alt Text" class="img xs-promo__img" src="<?php echo get_template_directory_uri(); ?>/contents/home-xs-promo-misc.jpg" />
        </figure>
        <div class="promo__c is-floated center">
            <div class="card text-white text-sm-center wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.25s" data-wow-offset="20">
                <div class="fs-60 mb-0 mb-sm-2"><?=$bannerTitle?></div>
                <a href="<?=$bannerLink?>" class="text-white ff-pp hidden-xs-down"><i class="icon icon-plus"></i><u><?=$bannerText?></u></a>
            </div>
        </div>
    </div>
</section>