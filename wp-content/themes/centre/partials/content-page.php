<?php /*
<?php the_content(); ?>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
*/ ?>


<?php if(!is_front_page() && !get_field('hide_title')):?>
<div class="section-gap">
<h1 class="title-decorate"><?php the_title(); ?></h1>
<?php the_content(); ?>
</div>
<?php endif; ?>
<?php
if(have_rows('flexible_contents')){
    while(have_rows('flexible_contents')){
        the_row();
        $templateFile = __DIR__.'/flexible-contents/'.get_row_layout().'.php';
        if(file_exists($templateFile)){
            require $templateFile;
        }
    }
}
?>
</div>

