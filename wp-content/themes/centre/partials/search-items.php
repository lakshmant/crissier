<div class="col-sm-4 d-sm-flex mb-3">
    <article id="article-<?= the_ID(); ?>" class="article-card article-card--sm shadowed bg-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20">
        <div class="article-card-inner">
            <?php
            $imgSrc = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );
            $img = \App\getImageManager()->resize( \App\getImageDirectoryPath($imgSrc[0]), \App\IMAGE_SIZE_NEWS_LARGE);
            if($img) : ?>
                <figure class="article-card__pic zoom-effect-holder mb-0"><img class="img img-full img-fluid zoom-effect" alt="Mag Image" src="<?=$img?>" /></figure>
            <?php endif; ?>
            <div class="article-card__c">
                <?php if(!empty($catName)) ?><div class="h5 promo-cat text-muted"><?=$catName?></div>
                <h2 class="promo-title pr-0 pr-sm-3 pr-md-4 pr-lg-3"><?php the_title(); ?></h2>
                <div class="promo-tags ff-os"><?php App\kd_related_terms($post->ID); ?></div>
            </div>
            <div class="shape-sq picto-holder is-floated right-bottom bg-primary d-inline-block">
                <i class="icon icon-light icon-plus card-icon mr-0"></i>
            </div>
        </div>
        <a href="<?php the_permalink(); ?>" class="link-stacked"></a>
    </article>
</div><!-- /.Search page posts -->