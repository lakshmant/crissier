<?php

/**
 * hide ACF dashboard on admin
 */
#require WPMU_PLUGIN_DIR.'/advanced-custom-fields-pro/cpt.php';

/**
 * https://www.advancedcustomfields.com/blog/acf-pro-5-5-13-update/
 * Faster load times!
 * disabe loading of scf custom tags
 */
add_filter('acf/settings/remove_wp_meta_box', '__return_true');



/**
 * add options page
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Options',
        'menu_title' 	=> 'Options',
        'redirect' 		=> false
    ));
}

add_action('admin_enqueue_scripts', function($hook){
    global $typenow;
    if($typenow == 'page'){
        if($hook == 'post.php' || $hook == 'post-new.php'){
            wp_enqueue_script('seic-acf-customize', plugin_dir_url( __FILE__ ).'assets/js/acf.js', [], false, true);
            wp_enqueue_style('seic-acf-customize-css', plugin_dir_url( __FILE__ ).'assets/css/acf.css', [], false);
        }
    }
});


function seic_acf_admin_enqueue_scripts() {
    if($_GET['page']=='acf-options-alert-options') {

        wp_enqueue_script('seic-acf-character-limit-js', plugin_dir_url(__FILE__) . 'assets/js/character-limit.js', [], false);
    }
}

add_action('acf/input/admin_enqueue_scripts', 'seic_acf_admin_enqueue_scripts');