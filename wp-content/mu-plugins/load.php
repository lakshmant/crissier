<?php
/*
Plugin Name: MU_Plugin Loader
Description: This is a loader for mu_plugins. The loader includes ACF
Author: Kabita
Version: 1.0
Author URI: http://procab.ch
*/
require WPMU_PLUGIN_DIR.'/app/vendor/autoload.php';
/*require WPMU_PLUGIN_DIR.'/advanced-custom-fields-pro/acf.php';
require WPMU_PLUGIN_DIR.'/advanced-custom-fields-table-field/acf-table.php';*/
require WPMU_PLUGIN_DIR.'/custom-post-types/cpt.php';
require WPMU_PLUGIN_DIR.'/acf-customize/admin.php';

/**
 * google map key
 */
 /*
add_action('acf/init', function(){
    acf_update_setting('google_api_key', 'AIzaSyBSq0Y5bxlRUhiePelWE9_OoJRIto30v3I');
});

*/

add_action('do_feed', function(){
    wp_die( __( 'No feed available, please visit the <a href="'. esc_url( home_url( '/' ) ) .'">homepage</a>!' ) );
}, 1);
add_action('do_feed_rdf', function(){ do_action('do_feed');}, 1);
add_action('do_feed_rss', function(){ do_action('do_feed');}, 1);
add_action('do_feed_rss2', function(){ do_action('do_feed');}, 1);
add_action('do_feed_atom', function(){ do_action('do_feed');}, 1);
add_action('do_feed_rss2_comments', function(){ do_action('do_feed');}, 1);
add_action('do_feed_atom_comments', function(){ do_action('do_feed');}, 1);