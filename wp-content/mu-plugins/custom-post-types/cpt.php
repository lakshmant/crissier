<?php
namespace App\Cpt;


/**
 * @param $name
 * @param array $args
 * https://developer.wordpress.org/resource/dashicons/#schedule
 */
function add_post_type($name, $args = array()){
    add_action('init', function() use($name, $args){
        $upper = ucwords($name);
        $name = strtolower(str_replace(' ', '_', $name));
        $args = array_merge(
            [
                'menu_icon' => 'dashicons-admin-home',
                'public'	=> true,
                'label'		=> "$upper",
                'labels'	=> ['add_new_item'=>"Add new $upper"],
                'supports' 	=> ['title','editor','thumbnail','page-attributes'],
                'exclude_from_search' => false,
                'capability_type'    => 'page'
            ],
            $args);
        register_post_type($name, $args);
    });
}
function add_taxonomy($name, $post_type, $args = array()){
    $name = strtolower($name);
    add_action('init', function() use($name, $post_type, $args){
        $args = array_merge(
            [
                'label' => ucwords($name),
                'show_ui' => true,
                'query_var' => true
            ],
            $args);
        //echo '<pre>';	var_dump($args);	echo '</pre>';
        register_taxonomy($name, $post_type,  $args);
    });
}


/**
 * update the permalinks after the add_post_type is created/modified
 */


add_post_type ( 'promotions', array (
    'name' => 'promotions',
    'menu_icon' => 'dashicons-format-aside',
    'supports' 	=> ['title','excerpt'],
    'label' => "Promotion",
    'hierarchical' => true,
    'taxonomies' => ['logo','promo_category'],
    'menu_icon'   => 'dashicons-format-image',
    //'has_archive' => 'presse'
));

add_post_type ( 'shop', array (
    'name' => 'shop',
    'menu_icon' => 'dashicons-format-aside',
    'supports' 	=> ['title','excerpt'],
    'label' => "Shop",
    'hierarchical' => true,
    'taxonomies' => ['logo','category'],
    'menu_icon'   => 'dashicons-calendar',
    //'has_archive' => 'presse'
));

add_post_type ( 'services', array (
    'name' => 'services',
    'menu_icon' => 'dashicons-format-aside',
    'supports' 	=> ['title','thumbnail'],
    'label' => "Services",
    'hierarchical' => true,
    /*'taxonomies' => ['logo'],*/
    'menu_icon'   => 'dashicons-calendar',
    //'has_archive' => 'presse'
));



 add_taxonomy('promo_cat','promotions',
     [
         'label' => 'Promotion Category',
         'show_admin_column' => true,
         'hierarchical' => true,
     ]
     );
add_taxonomy('logo',['promotions', 'shop'],[
    'hierarchical' => true,
    'show_admin_column' => true,
]);