<?php
/*
*  code to generate pdf for promotion list
*/


elseif($_GET['pdfType']=='promotionList'):
app_generate_pdf_promotionList();

}

function app_generate_pdf_promotionList(){

$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$obj_pdf->SetTitle("Promotions");
$obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->SetDisplayMode('fullpage', '','');
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, '5', PDF_MARGIN_RIGHT);
$obj_pdf->setPrintHeader(false);
$obj_pdf->setPrintFooter(false);
$obj_pdf->SetAutoPageBreak(TRUE, 10);
$obj_pdf->SetFont('helvetica', '', 12);
$obj_pdf->AddPage();
$style = "<link href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' rel='stylesheet'>
<style>
    body{

        border:none;
        font-size:0;
        line-height: 0.8;
        text-align: left;

    }
    p{
        font-size: 8px;
        line-height: 1;
        margin-top: 0;
        margin-bottom:0 ;
        margin-left:0;
        margin-rigth:0;
        padding-top:0;
        padding-bottom:0;
        padding-left:0;
        padding-right:0;
        font-weight: 400;
    }

    #promoPdf{
        font-family: 'Poppins', sans-serif;
    }
</style>";

$html = $style;



$args = [ 'post_type' => 'promotions', 'paged' => $paged, 'paged' => $paged,'posts_per_page' => 4,'post_status'=> 'publish'];

$query = new WP_Query( $args );

while($query->have_posts()): $query->the_post();
$bannerImage = get_field('image');
$pdfContents = get_field('pdf_contents');
//  var_dump($pdfContents);
// var_dump($pdfContents);
$bannerName = $pdfContents['name_commercial_center'];
$brandLogo = $pdfContents['logo'];
$barCodeImage = $pdfContents['barcode_image'];
$title = $pdfContents['title'];
$productName = $pdfContents['product_name'];
$productDescription = $pdfContents['description'];
$conditions = $pdfContents['conditions'];
$deadLine = $pdfContents['deadline'];




$deadlineTable = '<div><table style="padding-top:10px;padding-bottom:10px;"><tr><td style="padding-bottom:10px;padding-top:10px;padding-right:5px;padding-left:5px;">' . $deadLine . '</td></tr></table>';

    $center = '
    <table>
        <tr>
            <td style="text-align:left;margin-left:0;margin-right:0;margin-top: 0;margin-bottom:0;font-weight: 700;font-size: 12px;">' . $title . '<br>
            </td>
        </tr>

        <tr >
            <td style="margin-top: 10px;margin-bottom:0;font-size: 9px;font-weight: 600;">Nom du produit<br><br></td>
        </tr>
        <tr>
            <td><p style="margin-top:15px; margin-bottom:0;font-size: 8px;font-weight: 600;text-transform: uppercase">Description</p>' . $productDescription . '<br></td>

        </tr>
        <tr>
            <td><p style="margin-top:15px; margin-bottom:0; font-size: 8px;font-weight: 600;text-transform: uppercase">Conditions</p>' . $conditions . '<br></td>
        </tr>






    </table>


    ';
    $subtable1 = '
    <<table style="text-align: center;width:100%">
        <tr>
            <td style="font-size:8px;font-weight:600; text-align: left">
                ' . $bannerName . '<br>

            </td>
        </tr>
        <tr>
            <td>
                <img src="' . $brandLogo . '" alt="brand-logo" style="margin-top:10px;height: 60px;"><br>
            </td>
        </tr>
        <tr>
            <td>
                <img src="' . $bannerImage . '" alt="banner-image" style="margin-top:10px;height: 60px;"><br>
            </td>

        </tr>
        <tr>
            <td>
                <img src="' . $barCodeImage . '" alt="bar-code" style="margin-top:10px;height: 60px;"><br>
            </td>

        </tr>
    </table>
    ';

    $subtable2 = '<table style="text-align: left">
        <tr>
            <td>
                ' . $center . '
            </td>
        </tr>
    </table>';

    $subtable3 = '     <table style="text-align: center">
        <tr>
            <td style="text-align:left;font-size: 11px;font-weight: 600;border:1px solid #d2d1d1;margin-left:2px;margin-right:2px">
                ' . $deadlineTable . '
            </td>
        </tr>

        <tr>
            <td style="vertical-align: bottom;"><br><br><br><br><br><br><br><img alt="Brand Logo" src="' . site_url() . '/wp-content/themes/centre/assets/images/logo.png"  style="height: 60px;">
            </td>
        </tr>
    </table>';

    $startTable = '
    <div id="promoPdf">
        <table style="width:100%;background:#fff;border-collapse: collapse;border:1px solid #d2d1d1;margin-top:5%;margin-bottom:5%;margion-left:auto;margin-right:auto;"><tr><th style="width:25%"></th><th style="width:48%"></th><th style="width:26%"></th></tr><tr>';

                $centerName = ' <td style="width:25%;vertical-align:top;">
                    ' . $subtable1 . '
                </td>
                <td style="width: 47%;vertical-align:top;">
                    ' . $subtable2 . '
                </td>

                <td style="width: 26%;vertical-align:top;">
                    ' . $subtable3 . '
                </td>';

                ///    echo 'footer logo'.$footerLogo;
                $endTable = '</tr></table></div>';
    $html .= $startTable . $centerName . $endTable;

    endwhile;
    wp_reset_postdata();


    $pdfFilename = 'promotion-list-'.date("Y-m-d");
    $obj_pdf->writeHTML($html);
    $obj_pdf->Output(__DIR__ . '/pdf/' . $pdfFilename, 'F');
    $obj_pdf->Output($pdfFilename, 'I');
    }

