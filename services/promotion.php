<?php
/* Template Name: Promotion Page
 *
*/

/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 4/1/2019
 * Time: 2:50 PM
 */


require '../wp-load.php';
require_once('tcpdf/tcpdf.php');

if (isset($_GET['promoCount']) && isset($_GET['id'])) :
    $promotionBanner = get_field('promotion_banners', $_GET['id']);
    $pdfFilename = 'banner-promotion-' . $_GET['promoCount'] . '.pdf';
    /*    $filePath = __DIR__ . '/pdf/' . $pdfFilename;
        $fileUrl = home_url() . '/services/cache/' . $pdfFilename;
        $fileDir = '/pdf/' . $pdfFilename;
        if (file_exists($filePath)) {
            $fileUrl = home_url() . '/services/pdf/' . $pdfFilename;
            header("Location: " . $fileUrl);
            exit;

        } else {*/

    $i = 1;
    foreach ($promotionBanner as $banner) {
        if ($i == $_GET['promoCount']) {
            $bannerImage = !empty($banner['banner_image'])?$banner['banner_image']:home_url('/').'services/image/transparent-img.jpg';
            $pdfContents = $banner['pdf_contents'];
            $popupContents = $banner['popup_contents'];
            $productDescription = $popupContents['description'];

            $hideBanner = $pdfContents['hide_banner'];
            //  var_dump($pdfContents);
            // var_dump($pdfContents);
            //    $bannerName = $pdfContents['name_commercial_center'];
            $brandLogo = $pdfContents['logo'];
            $hideBanner = $pdfContents['hide_banner'];
            $barCodeImage = $pdfContents['barcode_image'];
            $title = $pdfContents['title'];
            $productName = $pdfContents['product_name'];
            $conditions = $pdfContents['conditions'];
            $deadLine = $pdfContents['deadline'];

            app_generate_pdf($bannerImage, $hideBanner, $brandLogo, $barCodeImage, $title, $productName, $productDescription, $conditions, $deadLine, $pdfFilename);
        }

        $i++;

        // echo 'counting loop '.$i;
        // }
    }

elseif(isset($_GET['pdfType'])):
    $pdfFilename = 'promotion-list.pdf';
    /*    $filePath = __DIR__ . '/pdf/' . $pdfFilename;
        $fileDir = '/pdf/' . $pdfFilename;
        if (file_exists($filePath)) {
            $fileUrl = home_url() . '/services/pdf/' . $pdfFilename;
            header("Location: " . $fileUrl);
            exit;

        } else {*/
    app_generate_promotion_pdf($pdfFilename);
// }

else :

    $promotionBanner = get_field('promotion_banners', $_GET['id']);
    $pdfFilename = 'promotion-' . $_GET['id'] . '.pdf';
    /*    $filePath = __DIR__ . '/pdf/' . $pdfFilename;
        $fileDir = '/pdf/' . $pdfFilename;
        if (file_exists($filePath)) {
            $fileUrl = home_url() . '/services/pdf/' . $pdfFilename;
            header("Location: " . $fileUrl);
            exit;

        } else {*/
    $logoTermID = get_the_terms($_GET['id'], 'logo');
    if (!empty($logoTermID[0]->term_id)) :
        $promoLogoImage = get_field('logo_image', 'logo_' . $logoTermID[0]->term_id);
        $brandLogo = $promoLogoImage['url'];
    endif;

    $bannerImage = get_field('image', $_GET['id']);
    $bannerImage = !empty($bannerImage)?$bannerImage:home_url('/').'services/image/transparent-img.jpg';

    $pdfContents = get_field('pdf_contents', $_GET['id']);
    $popupContents = get_field('popup_contents', $_GET['id']);

    // $bannerName = $pdfContents['name_commercial_center'];
    $barCodeImage = $pdfContents['barcode_image'];
    $hideBanner = $pdfContents['hide_banner'];
    $title = $pdfContents['title'];
    $productName = $pdfContents['product_name'];
    $productDescription = $popupContents['description'];
    $conditions = $pdfContents['conditions'];
    $deadLine = $pdfContents['deadline'];
    app_generate_pdf($bannerImage, $hideBanner, $brandLogo, $barCodeImage, $title, $productName, $productDescription, $conditions, $deadLine, $pdfFilename);
    //  }


endif;

/*
 *  code to generate pdf
 */
function app_generate_pdf($bannerImage, $hideBanner, $brandLogo, $barCodeImage, $title, $productName, $productDescription, $conditions, $deadLine, $pdfFilename){

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Promotions");
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '30', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(false);
    $obj_pdf->setPrintFooter(false);
    $obj_pdf->SetAutoPageBreak(false, 10);
    $obj_pdf->SetFont('helvetica', '', 12);
    $obj_pdf->AddPage();

    $style = "
<link href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' rel='stylesheet'>
<style>
    body{

        border:none;
        font-size:0;
        line-height: 0.8;
        text-align: left;

    }
    p{
    font-size: 8px;
    
    margin-top: 0 !important;
    margin-bottom:0 !important;
    margin-left:0;
    margin-right:0 ;
    padding-top:0 !important;
      padding-bottom:0 !important;
        padding-left:0;
          padding-right:0;
          font-weight: 400;
    }
   
    #promoPdf{
        font-family: 'Poppins', sans-serif;
        margin-top: 0;
    }
    table{
    white-space: unset;
    }
  
   
</style>";

    $conditionLabel = !empty($conditions)?'Conditions':'';
    $productDescriptionLabel = !empty($productDescription)?'Description':'';
    $transparentImage = home_url('/').'services/image/transparent-img.jpg';

    if(empty($hideBanner)):
        $bannerImageSection = '<table><tr><td><img src="' . $bannerImage . '" alt="banner-image" style="margin-top:10px; height:110px"></td></tr></table>';
    else :
        $bannerImageSection = '<table><tr><td><img src="' . $transparentImage . '" alt="banner-image" style="margin-top:10px; height:110px"></td></tr></table>';
    endif;

    $center = '
<table>
<tr>
<td style="text-align:left;margin-left:0;margin-right:0;margin-top: 0;margin-bottom:0;font-weight: 900;font-size: 15px;">' . $title . '<br>
</td>
</tr>

<tr >
                                <td style="margin-top: 10px;margin-bottom:0;font-size: 9px;font-weight: 600;">'.$productName.'<br/><br></td>
                                </tr>
                                <tr>
                                <td><p style="margin-top:15px; margin-bottom:0;font-size: 8px;font-weight: 600;text-transform: uppercase">'.$productDescriptionLabel.'</p>' . $productDescription . '<br></td>
                                   
                                   </tr>
                                    <tr>
                                    <td><p style="margin-top:15px; margin-bottom:0; font-size: 8px;font-weight: 600;text-transform: uppercase">'.$conditionLabel.'</p>' . $conditions .'</td>
</tr>

                                 
                                  
                              


</table>
           
                      
                          
        ';
    $subtable1 = '
<table style="text-align: center;width:100%">
                    <tr>
                        <td>
                            <img src="' . $brandLogo . '" alt="brand-logo" style="margin-top:10px;height: 60px;"><br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 11px;font-weight: 600;white-space: unset;padding-left:40px;height:100px">&nbsp;&nbsp;'.$deadLine.'</td>

                    </tr>
                    <tr>
                        <td>
                            <img src="' . $barCodeImage . '" alt="bar-code" style="margin-top:20px;height: 60px;"><br>
                        </td>

                    </tr>
                    </table>
';

    $subtable2 = '<table style="text-align: left">
                    <tr>
                        <td>
                        ' . $center . '
                            </td>
                        </tr>
                       </table>';

    $subtable3 = '     <table style="text-align: center;">
                   <tr>
                        <td style="font-size: 11px;font-weight: 600;height:20px">
                        ' . $bannerImageSection . '
                        </td>
                    </tr>
                   
                    <tr>
                        <td style="vertical-align: bottom;height:100px"><br><br><br><br><br><br><br><img alt="Brand Logo" src="' . site_url() . '/wp-content/themes/centre/assets/images/logo-pdf.png"  style="height: 60px;"><br>
                        </td>
                    </tr>
                   </table>';

    $startTable = '
<div id="promoPdf">

    <table style="width:100%;background:#fff;border-collapse: collapse;border:1px solid #d2d1d1;margin-left:auto;margin-right:auto;"><tr><th style="width:30%"></th><th style="width:47%"></th><th style="width:24%"></th></tr><tr>';

    $centerName = ' <td style="width:29%;vertical-align:top;">
 '.$subtable1.'
 </td>
             <td style="width: 47%;vertical-align:top;height:260px;">
                ' . $subtable2 . '
                </td>
                
                <td style="width: 24%;vertical-align:top;">
           ' . $subtable3 . '
            </td>';

    ///    echo 'footer logo'.$footerLogo;

    $endTable = '</tr></table></div>';


    $html = $style . $startTable . $centerName . $endTable;
    $obj_pdf->writeHTML($html);
    $obj_pdf->Output(__DIR__ . '/pdf/' . $pdfFilename, 'F');
    $obj_pdf->Output($pdfFilename, 'I');
}

/*
 *  Generate Promotion list pdf
 */

function app_generate_promotion_pdf($pdfFilename) {

    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $obj_pdf->SetTitle("Promotions");
    $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '30', PDF_MARGIN_RIGHT);
    $obj_pdf->setPrintHeader(false);
    //$obj_pdf->setHeaderTemplateAutoreset(true);
    $obj_pdf->setPrintFooter(false);
    $obj_pdf->SetAutoPageBreak(TRUE, 5);
    $obj_pdf->SetFont('helvetica', '', 12);
    $obj_pdf->AddPage();

    $style = "
<link href='https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900' rel='stylesheet'>
<style>
    body{

        border:none;
        font-size:0;
        line-height: 0.8;
        text-align: left;

    }
    p{
    font-size: 8px;
    
    margin-top: 0 !important;
    margin-bottom:0 !important;
    margin-left:0;
    margin-right:0 ;
    padding-top:0 !important;
      padding-bottom:0 !important;
        padding-left:0;
          padding-right:0;
          font-weight: 400;
    }
   
    #promoPdf{
    margin-top: 0;
        font-family: 'Poppins', sans-serif;
    }
    table{
    white-space: unset;
    }
  
</style>";

    $html = $style;
    $args = ['post_type'=>'promotions', 'posts_per_page'=>-1, 'post_status'=>"publish"];
    $the_query = new WP_Query( $args );
    $countSection=1;
    if($the_query->have_posts()):
        while ($the_query->have_posts()): $the_query->the_post();

            $bannerImage = !empty(get_field('image'))?get_field('image'):home_url('/').'services/image/transparent-img.jpg';
            $pdfContents = get_field('pdf_contents');
            //   $bannerName = $pdfContents['name_commercial_center'];
            $barCodeImage = $pdfContents['barcode_image'];
            $title = $pdfContents['title'];
            $productName =  !empty($pdfContents['product_name'])?$pdfContents['product_name']:'';
            $conditions = $pdfContents['conditions'];
            $deadLine = $pdfContents['deadline'];
            $logoTermID = get_the_terms($the_query->post->ID,'logo');
            $popupContents = get_field('popup_contents');
          //  var_dump($popupContents);
            $productDescription = $popupContents['description'];
            $conditions = $pdfContents['conditions'];
            $conditionLabel = !empty($conditions)?'Conditions':'<p></p><p></p><p></p>';

            $productDescLabel = !empty($productDescription)?'Description':'<p></p><p></p>';
            $transparentImage = home_url('/').'services/image/transparent-img.jpg';

            $hideBanner = $pdfContents['hide_banner'];
            if(!empty($logoTermID[0]->term_id)) :
                $promoLogoImage = get_field('logo_image', 'logo_'.$logoTermID[0]->term_id);
                $brandLogo = $promoLogoImage['url'];
            endif;

            if(empty($hideBanner)):
                $bannerImageSection = '<table><tr><td><img src="' . $bannerImage . '" alt="banner-image" style="margin-top:10px;height: 110px;"></td></tr></table>';
            else:
                $bannerImageSection = '<table><tr><td><img src="' . $transparentImage . '" alt="banner-image" style="margin-top:10px;height: 110px;"></td></tr></table>';
            endif;

            $center = '
<table>
<tr>
<td style="text-align:left;margin-left:0;margin-right:0;margin-top: 0;margin-bottom:0;font-weight: 900;font-size: 15px;">' . $title . '<br>
</td>
</tr>

<tr >
                                <td style="margin-top: 10px;margin-bottom:0;font-size: 9px;font-weight: 600;">'.$productName.'<br><br/></td>
                                </tr>
                                <tr>
                                <td><p style="margin-top:15px; margin-bottom:0;font-size: 8px;font-weight: 600;text-transform: uppercase">'.$productDescLabel.'</p>' . $productDescription . '<br></td>
                                   
                                   </tr>
                                    <tr>
                                    <td><p style="margin-top:15px; margin-bottom:0; font-size: 8px;font-weight: 600;text-transform: uppercase">'.$conditionLabel.'</p>' . $conditions . '<br></td>
</tr>

                                 
                                  
                              


</table>
           
                      
                          
        ';
            $subtable1 = '
<table style="text-align: center;width:100%">
                    <tr>
                        <td>
                            <img src="' . $brandLogo . '" alt="brand-logo" style="margin-top:10px;height: 60px;"><br>
                        </td>
                    </tr>
                    <tr>
                        <td  style="font-size: 11px;font-weight: 600;white-space: unset;padding-left:40px;height:80px">'.$deadLine.'</td>

                    </tr>
                    <tr>
                        <td>
                           <br><br> <img src="' . $barCodeImage . '" alt="bar-code" style="margin-top:20px;height: 60px;"><br>
                        </td>

                    </tr>
                    </table>
';

            $subtable2 = '<table style="text-align: left">
                    <tr>
                        <td>
                        ' . $center . '
                            </td>
                        </tr>
                       </table>';

            $subtable3 = '     <table style="text-align: center">
                   <tr>
                        <td style="font-size: 11px;font-weight: 600;height:20px;">
                        ' . $bannerImageSection . '
                        </td>
                    </tr>
                   
                    <tr>
                        <td style="vertical-align: bottom;height:100px"><br><br><br><br><br><br><br><img alt="Brand Logo" src="' . site_url() . '/wp-content/themes/centre/assets/images/logo-pdf.png"  style="height: 60px;"><br>
                        </td>
                    </tr>
                   </table>';

            $startTable = '


<div id="promoPdf">

    <table class="single-table" style="width:100%;background:#fff;border-collapse: collapse;border:1px solid #d2d1d1;margin-left:auto;margin-right:auto;"><tr><th style="width:30%"></th><th style="width:47%"></th><th style="width:24%"></th></tr><tr>';

            $centerName = ' <td style="width:29%;vertical-align:top;">
                ' . $subtable1 . '
            </td>
             <td style="width: 47%;vertical-align:top; height:260px;">
                ' . $subtable2 . '
                </td>
                
                <td style="width: 24%;vertical-align:top;">
           ' . $subtable3 . '
            </td>';
            ///    echo 'footer logo'.$footerLogo;
            ///

        if($countSection>3) {
            $endTable = '</tr></table><br><br><br><br><br></div>';
        } else {
            $endTable = '</tr></table><br/><br><br><br><br/></div>';
        }
            $countSection++;
            $html .= $startTable . $centerName . $endTable;
        endwhile;

        wp_reset_query();

    endif;


    $obj_pdf->writeHTML($html);
//echo $html;
    $obj_pdf->Output(__DIR__ . '/pdf/' . $pdfFilename, 'F');
    $obj_pdf->Output($pdfFilename, 'I');

}